<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width:device-width, user-scalable=no, initial-scale=1, maximum-scale=1, minimum-scale=1.0">
		<link href="https://fonts.googleapis.com/css?family=PT+Sans:400,400i,700,700i|Roboto" rel="stylesheet">  
		<link rel="stylesheet" href="../css/fontello.css">
		<link rel="stylesheet" href="../css/bootstrap.css">
		<link rel="stylesheet" href="../css/bootstrap.min.css">
		<link rel="stylesheet" href="../css/estilos2.css">
		<title>Recuperación contraseña comercio PymCash</title>
	</head>
	<body>
		<main>
			<div class="recoverpass">
				<div class="container">
					<div class="row info">
						<p class="h2 titulo col-12">¿Olvidaste tu contraseña?</p>
						<div class="texto col-12">
							<p>Ingresa el rut de tu comercio y enviaremos un email al correo asociado</p>
						</div>
				
						<div class="form-group col-12">
							<input type="text" name="rut" id="rut" placeholder="RUT" required="required">
		               	 </div>
		               	<div class="boton-sendmail col-12">
			               <button type="submit" name="submit" required="required">
							Enviar Correo
							</button>
		               	</div>
					</div>
				</div>
			</div>
		</main>
		<script src="../js/jquery-3.3.1.min.js"></script>
		<script src="../js/menus.js"></script>
		<script src="../js/popper.min.js"></script>
		<script src="../js/boostrap.min.js"></script>
	</body>
</html>
