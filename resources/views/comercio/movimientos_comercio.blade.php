@extends('layout_comercio_2')

@section('content')
<header>
		<div class="header-movimientos">
			<div class="container">
				<div class="row barra col-12">
					<div class="icono-izquierda col-md-4 col-2">
						<a href="{{ route('billetera') }}"><i class="icon-left-open"></i></a>
					</div>
					<div class="titulo col-md-8 col-10">
						Mis Movimientos
					</div>
				</div>
			</div>
		</div>
	</header>
	<div class="main-movimientos">
		<div class="container">
			<div class="separador"></div>
			<div class="row tabla">
				<table class="table table-striped">
				  <thead class="bordes">
				    <tr class="cabecera">
				      <th scope="col">Fecha</th>
				      <th scope="col">Monto</th>
				      <th scope="col">Detalle</th>
				      <th scope="col">Cantidad PC</th>
				      <th scope="col">N° Boleta</th>
				    </tr>
				  </thead>
				  <tbody>
						@if ($movimientos->first()==NULL)
						<tr>
				      <th class="detalle" scope="row">No hay movimientos</th>
				      <td class="fecha"></td>
				      <td class="detalle"></td>
				      <td class="cuenta"></td>
				      <td class="boleta"></td>
				    </tr>			
						@else

							@foreach ($movimientos as $movimiento)		
								<tr>									
									<th class="fecha" scope="row">{{ date('d-m-Y',strtotime($movimiento->fecha)) }}
									<br> {{ date('h:i:s A',strtotime($movimiento->fecha)) }}</th>
									<td class="monto">{{ $movimiento->monto }}</td>
									<td class="detalle">{{ $movimiento->detalle }}</td>
										@if ( (strpos(strtolower($movimiento->detalle),"venta")!==false) && $movimiento->pymcash>0 )
											<td class="cuenta">-{{ $movimiento->pymcash }} PCash</td>
										@elseif ( (strpos(strtolower($movimiento->detalle),"compra")!==false) && $movimiento->pymcash>0 )
											<td class="cuenta">+{{ $movimiento->pymcash }} PCash</td>
										@else
											<td class="cuenta">{{ $movimiento->pymcash }} PCash</td>
										@endif									
									<td class="boleta">{{ $movimiento->boleta }}</td>
								</tr>
							@endforeach

						@endif
				  </tbody>
				</table>
			</div>
			<div class="saldo">
				<p><img src="../iconos/PC-01.png" alt=""> Saldo: {{ $user->getBilletera() }} </p>
			</div>
		</div>
	</div>
@endsection