<markers>
@foreach($usuarios as $usuario)
<marker 
        name = "{{ $usuario->getModel()->nombre }} {{ $usuario->getModel()->apellido }}"
        address = "{{ $usuario->direccion }}"
        lat = "{{ $usuario->latitud }}"
        lng = "{{ $usuario->longitud }}"
/>
@endforeach
</markers>