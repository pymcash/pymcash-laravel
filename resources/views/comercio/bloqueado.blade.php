@extends('layout_comercio')

@section('content')
<div class="main-notificaciones">
		<div class="container">
			<div class="row">
				<div class="botones d-flex justify-content-center col-12 col-md-10 mt-3">
					<div class="btn-group" role="group" aria-label="Basic example">
				  		<button type="button" class="btn btn-secondary izquierda">
				  			<a href="{{ route('notificaciones') }}">Notificaciones</a>
				  		</button>
				  		<button onclick="window.location.href='{{ route('mensajeria') }}'" type="button" class="btn btn-secondary derecha">
				  			<a href="{{ route('mensajeria') }}">Búsqueda</a>
				  		</button>
				  		<button onclick="window.location.href='{{ route('chat_lista') }}'" type="button" class="btn btn-secondary derecha activo">
				  			<a href="{{ route('chat_lista') }}">Chats</a>
				  		</button>
					</div>
				</div>
				<div class="botones2 d-flex justify-content-end col-8-inverse col-md-2 mt-2">
					<div class="commerceblock">
						<a href="{{ route('bloqueados') }}">
							<img src="../iconos/bloquear.png" width="22" height="22">
							<p class="texto">Miembros Bloqueados</p>
						</a>
					</div>
				</div>
			</div>
		</div>
		
			<div class="container">
				<div class="row bloqueados">
                    @if (count($bloqueados)>0)
                        @foreach ($bloqueados as $bloqueado)
                        <?php 
                            $model = $bloqueado->getModel();
                            $nombre = $model->nombre." ".$model->apellido;
                            $charset='ISO-8859-1'; // o 'UTF-8'
							$str = iconv($charset, 'ASCII//TRANSLIT',lcfirst($nombre[0]));
							$icon = preg_replace("/[^A-Za-z0-9 ]/", '', $str);
                        
                        ?>
                        <!-- Bloqueado #1 -->
                        <div class="bloqueado d-flex justify-content-center col-12 col-md-12">
                            <div class="col-2 col-md-2 imagen">
                                <img src="../iconos/letras/{{ $icon }}.png">
                            </div>
                            <div class="col-8 col-md-6 content">
                                <p class="titulo">{{ $nombre }}</p>
                                <button class="btn btn-secondary d-block d-md-none">
                                    <a href="{{ route('desbloquear',['rut' => $model->rut  ]) }}"><i class="icon-lock-open">Desbloquear</i></a>
                                </button>
                            </div>
                            <div class="col-md-2 d-none d-md-block">
                                <a href="{{ route('desbloquear',['rut' => $model->rut  ]) }}" class="btn btn-secondary"><i class="icon-lock-open">Desbloquear</i></a>
                            </div>
                        </div>
                        @endforeach

                    @endif




				</div>
			</div>
		</div>
@endsection