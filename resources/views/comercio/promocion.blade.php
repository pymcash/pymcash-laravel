@extends('layout_comercio')

@section('content')
<div class="promodecide">
		<div class="titulo-enviar">
			<p>Enviar Promociones</p>
		</div>
		<div class="textos">
			<p class="texto1">Cuéntanos, ¿Cómo deseas enviar tu promoción?</p>
			<p class="texto2"><strong>Elije Chat:</strong> Si desea comunicarse a través del chat con los miembros del sistema</p>
			<p class="texto3"><strong>Elija Email:</strong> Si desea enviar promoción vía correo a los miembros del sistema</p>
		</div>
		<div class="form-enviarpromo">
		    <form>
				<select class="form-control" name="area" onChange="location = form.area.options[form.area.selectedIndex].value;">
					<option value="comerciopromodecide.php">Elija una Opción</option>
					<option value="{{ route('promocion_chat') }}">Tomar contacto por Chat</option>
					<option value="{{ route('promocion_mail') }}">Enviar por correo</option>
				</select>
			</form>
		</div>
</div>    
@endsection