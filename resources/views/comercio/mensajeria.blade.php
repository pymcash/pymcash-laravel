@extends('layout_comercio')

@section('content')
<div class="main-mensajeria">
		<div class="container">
			<div class="row">
				<div class="botones d-flex justify-content-center col-12 col-md-10 mt-3 mb-4">
					<div class="btn-group" role="group" aria-label="Basic example">
				  		<button type="button" class="btn btn-secondary izquierda">
				  			<a href="#">Notificaciones</a>
				  		</button>
				  		<button onclick="window.location.href='{{ route('mensajeria') }}'" type="button" class="btn btn-secondary centro activo">
				  			<a href="#">Busqueda</a>
				  		</button>
				  		<button onclick="window.location.href='{{ route('chat_lista') }}'" type="button" class="btn btn-secondary derecha">
				  			<a href="{{ route('chat_lista') }}">Chats</a>
				  		</button>
					</div>
				</div>
				<div class="botones2 d-flex justify-content-end col-8-inverse col-md-2 mt-2">
					<div class="commerceblock activobloqueados">
						<a href="{{ route('bloqueados') }}">
							<img src="../iconos/bloquear.png" width="22" height="22">
							<p class="texto">Miembros Bloqueados</p>
						</a>
					</div>
				</div>
			</div>
		</div>
		
		<div class="container">
			<div class="row barrabusqueda">
				 <div class="barra input-group col-md-12 d-flex justify-content-center">
	                <input type="text" id="busqueda" name="busqueda" class="form-control barra" placeholder="Buscar contacto" aria-describedby="basic-addon1">
	                <i class="icon-search"></i>
                </div>
			</div>
		</div>

		<br/>
		<div class="container">
			<div class="row contactos">
				
				@if (isset($miembros))
					@foreach($miembros as $miembro)
						
						<a href="{{ route('chat',['rut' => $miembro->rut ]) }}" class="contacto d-flex justify-content-center col-12 col-md-12">
						<div class="col-2 col-md-4 imagen">
						<?php  
							$charset='ISO-8859-1'; // o 'UTF-8'
							$str = iconv($charset, 'ASCII//TRANSLIT',lcfirst($miembro->nombre[0]));
							$icon = preg_replace("/[^A-Za-z0-9 ]/", '', $str);
						?>
						
							<img src="../iconos/letras/{{ $icon }}.png">
						</div>
						<div class="col-10 col-md-8 content">
							<p class="titulo">{{ $miembro->nombre }} {{ $miembro->apellido }} </p>
							<p class="texto"></p>
						</div>
						</a>						
					@endforeach
				@endif

			</div>
		</div>

	</div>
@endsection