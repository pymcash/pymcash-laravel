@extends('layout_comercio')

@section('content')

	<div class="contenedor">
		<div class="titulo">
			<p>Venta</p>
		</div>
		<div class="main-venta">
			<div class="titulo2">
				<p>¡Registre su venta!</p>
			</div>
			<form name="form" action="" method="post">
			<form action="{{ route('vender') }}" method="post">
				<input type="hidden" name="_method" value="POST">
				<input type="hidden" name="_token" value="{{ csrf_token() }}">
				@if ( ($errors->has('pymcash'))  )			
						<div class="form-group alert">                                
							<p>Insuficientes pymcash para realizar ésta operación.</p> 
						</div>
            	@endif    
				<div class="monto">
					<div class="texto">
						<p>Monto de su venta</p>
					</div>
					<div class="input">
						<input type="number" placeholder="" value="{{ old('monto') }}" name="monto">
					</div>
				</div>
				<div class="numeromiembro">
					<div class="texto">
						<p>N° de miembro</p>
					</div>
					<div class="input">
						<input type="number" placeholder="" value="{{ old('miembro') }}"   name="miembro">
					</div>
					@if ( ($errors->has('miembro')) || ($errors->has('rut')) )			
						<div class="form-group alert">                                
							<p>El miembro ingresado no existe</p> 
						</div>
            		@endif     
				</div>
				<div class="numeroboleta">
					<div class="texto">
						<p>N° de Boleta</p>
					</div>
					<div class="input">
						<input type="number" placeholder="" name="boleta" value="{{ old('boleta') }}" >
					</div>
				</div>
				<div class="boton">
					<!-- <button type="submit" class="boton">Aceptar</button> -->
					<input type=button onclick="pregunta()" value="Enviar"> 
				</div>
			</form>
		</div>
	</div>
	<script language="JavaScript">
		function pregunta(){
		    if (confirm('¿Está seguro de los datos ingresados?')){
		       document.form.submit()
		    }
		}
	</script> 
@endsection