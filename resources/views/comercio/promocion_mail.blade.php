@extends('layout_comercio')

@section('content')
<div class="main-promomail">
			<div class="titulo-1">
				<p>Enviar Promociones</p>
			</div>
			<div class="texto-1">
				<p>Este mensaje será enviado a todos los miembros de PymCash vía Email</p>
			</div>

		<div class="contenedor">
			<form id="main-contact-form" name="contact-form" method="post" action="sendpromo.php">
				<div class="titulomensaje">
					<input type="text" name="titulo" id="titulo" placeholder="Título de su promoción" required="required">
				</div>
				<div class="textomensaje">
					<div class="textarea">
						<label for="mensaje">Mensaje: </label>
						<textarea class="mensaje" name="msg" id="msg" required="required" placeholder="Escriba su promoción" class="form-control"></textarea>
					</div>
				</div>

				<div class="boton-submit">
	                <button type="submit" name="submit" class="login100-form-btn" required="required">
						Enviar Promocion
					</button>
				</div>
			</form>

		</div>
	</div>
@endsection