@extends('layout_comercio')

@section('content')
<div class="main-notificaciones">
		<div class="container">
			<div class="row">
				<div class="botones d-flex justify-content-center col-12 col-md-10 mt-3 mb-4">
					<div class="btn-group" role="group" aria-label="Basic example">
				  		<button type="button" class="btn btn-secondary izquierda">
				  			<a href="#">Notificaciones</a>
				  		</button>
                          <button onclick="window.location.href='{{ route('mensajeria') }}'" type="button" class="btn btn-secondary centro">
				  			<a href="{{ route('mensajeria') }}">Busqueda</a>
				  		</button>
				  		<button onclick="window.location.href='{{ route('chat_lista') }}'" type="button" class="btn btn-secondary derecha activo">
				  			<a href="{{ route('chat_lista') }}">Chats</a>
				  		</button>
					</div>
				</div>
                <div class="botones2 d-flex justify-content-end col-8-inverse col-md-2 mt-2">
					<div class="commerceblock activobloqueados">
						<a href="{{ route('bloqueados') }}">
							<img src="../iconos/bloquear.png" width="22" height="22">
							<p class="texto">Miembros Bloqueados</p>
						</a>
					</div>
				</div>
			</div>
		</div>
		
			<div class="container">
				<div class="row notificaciones">


                    @if (count($chats))
                            @foreach($chats as $chat)
                            
                                <?php 
                                    $rut_miembro = "";
                                    if ( $chat->rut_1 !== $auth->rut )
                                        $rut_miembro = $chat->rut_1;
                                    else 
                                        $rut_miembro = $chat->rut_2;
                                    $miembro = $user::where("rut","{$rut_miembro}")->first()->getModel(); 
                                    
                                    
                                    $mi_rut = $auth->rut;
                                    $msg = $Mensaje::where("chat_id","{$chat->id}")->orderBy("time","DESC")->first();                                     
                                    
                                    // FILTRANDO ULIIMO MENSAJE

                                    $msgs = $EliminarMensaje::whereRaw("rut = '$mi_rut'")->get(['message_id'])->groupBy('message_id')->keys()->all();                        
                                    if (count($msgs)>0)
                                        $msg = $msg->whereNotIn('id',$msgs)->orderBy("time","DESC")->first();
                                    
                                    if ($msg!==NULL)                                    
                                        $msg = $msg->message;
                                    else    
                                        $msg = '';                                        
                                    


                                    $count = count($Mensaje::whereRaw("chat_id = {$chat->id} AND rut = '{$rut_miembro}'  AND status = 'unread' ")->get());

                                    $charset='ISO-8859-1'; // o 'UTF-8'
                                    $str = iconv($charset, 'ASCII//TRANSLIT',lcfirst($miembro->nombre[0]));
                                    $icon = preg_replace("/[^A-Za-z0-9 ]/", '', $str);
                                ?>
                                <!-- Notificacion #1 -->                          
                                <div class="notificacion d-flex justify-content-center col-12 col-md-12">
                                    <div class="col-2 col-md-2 imagen">
                                        <img src="../iconos/letras/{{ $icon }}.png">
                                    </div>
                                    <div class="col-8 col-md-6 content">
                                        <p class="titulo">{{ $miembro->nombre }} {{ $miembro->apellido }}</p>
                                        <p class="texto">{{ $msg }}</p>
                                        <button class="btn btn-secondary d-block d-md-none">
                                            <a href="{{ route('chat',['rut' => $miembro->rut ]) }}">Abrir chat</a>
                                        </button>
                                    </div>
                                    <div class="col-md-2 d-none d-md-block">
                                        <a href="{{ route('chat',['rut' => $miembro->rut ]) }}" class="btn btn-secondary">Abrir chat</a>
                                    </div>
                                    <div class="col-2 col-md-2 iconos">
                                        <a href="{{ route('eliminar_chat',['rut' => $miembro->rut ]) }}" class="close"><img src="../iconos/close.png"></a>
                                        @if ($count>0)
                                            <span class="badge badge-danger bluealert">{{ $count }}</span>
                                        @else
                                            <br>
                                        @endif
                                        
                                        <p class="datetime">{{ date('h:i:s A',strtotime($chat->last_time)) }}</p>
                                    </div>
                                </div>


                            @endforeach
                    @endif



				</div>
			</div>
		</div>
@endsection