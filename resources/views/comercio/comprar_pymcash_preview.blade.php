@extends('layout_comercio')

@section('content')
<div class="contenedor-comprapymcashpreview">
    <div class="titulo">
        <p>Vista previa de su compra</p>
    </div>
    <div class="descripcion">
        <p>El precio especificado es por pack, cada pack posee 10 PymCash</p>
    </div>
    <div class="main-venta">
            <div class="imagen">
                <img src="../img/logopymcash2.png" alt="">
            </div>
        <div class="texto-sm">
            <p>Detalle de compra:</p>
        </div>

        <form action="{{ route('pagar') }}" method="post">
            <input type="hidden" name="_method" value="POST">
			<input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div class="detallecompra">
                <!-- items de compra -->
                <div class="descripcion">

                    @for ($i = 0 ; $i<$cantidad ;$i++)                        
                        <p class="numero">1</p>
                        <p class="tituloitem">Pack PymCash (10 PymCash)</p>
                        <p class="monto">3.000 CLP</p>
                    @endfor
                    
                    
                </div>

                <div class="npaquetes">
                    <div class="numero">{{ $cantidad }}</div>
                    <div class="packs" id="pymcash">Packs de Pymcash</div>
                    <div class="icono"><img src="../iconos/PC-01.png" alt="Icono Moneda PCash"></div>
                </div>

                <div class="pago">
                    <p class="texto-stotal" id="subtotal">Subtotal:</p>
                    <p class="subtotal">${{ $monto }} + IVA</p>

                    <p class="texto-total" id="ltotal">Total:</p>
                    <p class="total">${{ $total }} CLP</p>
                </div>
                <input type="hidden" name="cantidad" value="{{ $cantidad }}">

            </div>
            
            <div class="boton">
                <button onclick="location.href = 'consumidorespanel.html';" type="submit" class="boton">Continuar a pagar</button>
            </div>
        </form>
    </div>
</div>
@endsection