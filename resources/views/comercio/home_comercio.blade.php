@extends('layout_comercio')

@section('content')
<div class="main">
			<section class="comercio">
				<div class="contenedor">
					<!-- Tuerca en pantalla grande -->
					<div class="d-none d-md-block tuerca">
						<a href="#" id="tuerca"><img src="../iconos/tuercablanca.png" width="22" height="22"></a>
					</div>

					<div class="col-md-3 barra-lateral-izquierda" id="barra-lateral-izquierda">
						<nav>
							<a href="{{ route('mis_datos') }}"><i class="icon-user"></i>Ficha Personal</a>
							<a href="{{ route('cambiar_contraseña') }}"><i class="icon-switch"></i>Cambiar Contraseña</a>
							<hr>
							<a href="#">Términos y condiciones</a>
							<a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Cerrar Sesión</a>
						</nav>
					</div> 

					<a href="#" class="fondo-enlace" id="fondo-enlace"></a>
					<!-- Foto usuario -->
					<div class="foto col-12">
						<img src="../img/MercadoChamartin.jpg" alt="usuario">
					</div>
					<div class="texto col-12">
						<h3 class="nombre">{{ $user->getModel()->nombre_fantasia }}</h3>
					</div>
				</div>
			</section>

			<section class="menu-principal">
				<div class="container">
					<div class="row">
						<div class="col-md-3 col-sm-6 col-6">
							<button onclick="window.location.href='/vender'" class="btn btn1">
								<a href="{{ route('vender') }}"><img src="../iconos/vender.png" alt=""></a>
							</button>
							<p class="titulo1">Vender</p>
						</div>
						<div class="col-md-3 col-sm-6 col-6">
							<button onclick="window.location.href='/billetera'" class="btn btn2">
								<a href="{{ route('billetera') }}"><img src="../iconos/micartera.png" alt=""></a>
							</button>
							<p class="titulo2">Mi Billetera</p>
						</div>
						<div class="col-md-3 col-sm-6 col-6">
							<button onclick="window.location.href='/comprar'" class="btn btn3">
								<a href="{{ route('comprar') }}"><img src="../iconos/comprarpymcash.png" alt=""></a>
							</button>
							<p class="titulo3">Comprar Pymcash</p>
						</div>
						<div class="col-md-3 col-sm-6 col-6">
							<button onclick="window.location.href='/promocion'" class="btn btn4">
								<a href="{{ route('promocion') }}"><img src="../iconos/enviarpromociones.png" alt=""></a>
							</button>
							<p class="titulo4">Enviar Promociones</p>
						</div>
					</div>
				</div>
			</section>
		</div>    
@endsection