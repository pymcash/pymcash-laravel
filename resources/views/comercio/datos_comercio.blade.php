@extends('layout_comercio_2')

@section('content')
<header>
		<div class="header-commerce">
			<div class="container">
				<div class="row barra col-12">
					<div class="icono-izquierda col-md-4 col-2">
						<a href="{{ route('home') }}"><i class="icon-left-open"></i></a>
					</div>
					<div class="titulo col-md-4 col-10">
						Datos de su comercio
					</div>
				</div>
			</div>
		</div>
	</header>
	<div class="main-datacommerce">
		<div class="container">
			<div class="row datos">
				
				<form id="main-contact-form col-12" class="contact-form" name="contact-form" method="post" action="#" enctype="multipart/form-data"> 
					<input type="hidden" name="_method" value="PUT">
    				<input type="hidden" name="_token" value="{{ csrf_token() }}">
	                 <div class="imagen col-12">
						<img src="../img/MercadoChamartin.jpg" id="DNI" width="200px" name="DNI" alt="Documento de identidad">
					</div>
                  	
                  	<div class="rutcomercio">
                        <input type="text" name="rutcomercio" id="rutcomercio" placeholder="RUT del comercio" value="{{ $user->rut }}" readonly>
                  	</div>
                    
                    <div class="nombrefantasia">
				 		<input type="text" name="nombrefantasia" id="nombrefantasia" placeholder="Nombre fantasía del comercio"  value="{{ $user->getModel()->nombre_fantasia }}" readonly>
					</div>                    
                    
                    <div class="razonsocial">
						<input type="text" name="razonsocial" id="razonsocial" placeholder="Razón social del comercio" value="{{ $user->getModel()->razon_social }}" readonly>
                    </div>					

					<div class="direccion">
						<input type="text" name="direccion" id="direccion" required="required" placeholder="Direccion" value="{{ $user->direccion }}"  >
					</div>
    
	                <div class="comuna">
	                 	<input type="text" name="comuna" id="comuna" required="required" placeholder="Comuna" value="{{ $user->comuna }}" >
	                </div>
                     
                     <div class="telefono">
                     	<input type="text" name="telefono" id="telefono" required="required" placeholder="Teléfono" value="{{ $user->telefono }}" >
                     </div>
					
					<div class="mail">
						@if ($errors->has('email'))
							<input type="text" name="email" id="email" class="input100" required="required" value="{{ old('email') }}" placeholder="Email">
							<div class="form-group text-center">                                                                        
								<strong style="color:#bd1f33" >{{ $errors->first('email') }}</strong>                                                                       
							</div>                                                
						@else
							<input type="text" name="email" id="email" class="input100" required="required" value="{{ $user->email }}" placeholder="Email">
                    	@endif     
					</div>
                     
                	<div class="boton">
                		<button type="submit" name="save" id="save">
						Guardar Cambios
						</button>
                	</div>
                 
				</form>


			</div>
		</div>
	</div>
@endsection