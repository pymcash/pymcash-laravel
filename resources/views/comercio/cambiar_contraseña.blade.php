@extends('layout_comercio_2')

@section('content')
<header>
		<div class="header-changepassword">
			<div class="container">
				<div class="row barra col-12">
					<div class="icono-izquierda col-md-4 col-2">
						<a href="{{ route('home') }}"><i class="icon-left-open"></i></a>
					</div>
					<div class="titulo col-md-8 col-10">
						Cambiar contraseña
					</div>
				</div>
			</div>
		</div>
	</header>
	<div class="divisor"></div>
	<div class="contenedor-changepassword">
		<form method="post" action="{{ route('cambiar_contraseña') }}">
			<input type="hidden" name="_method" value="PUT">
    		<input type="hidden" name="_token" value="{{ csrf_token() }}">
			@if ($errors->has('password'))			
				<div class="form-group text-center">                                                                        
					<strong style="color:#bd1f33" >{{ $errors->first('password') }}</strong>                                                                       
				</div>                                                			
            @endif     
        	<div class="form-group">
				<input type="password" pattern=".{8,12}" required title="8 a 12 caracteres" name="password" id="password" required="required" placeholder="Nueva contraseña">
            </div> 
            <div class="form-group">
				<input type="password" pattern=".{8,12}" required title="8 a 12 caracteres" placeholder="Confirmar contraseña" name="password_confirmation" id="password_confirmation" required="required">
            </div>

                    
            <div class="form-group">
                <button type="submit" value="cambiarpass" id="cambiarpass" name="cambiarpass">
				Cambiar Contraseña
				</button>
			</div>

		</form>
	</div>	    
@endsection