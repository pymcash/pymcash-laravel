<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width:device-width, user-scalable=no, initial-scale=1, maximum-scale=1, minimum-scale=1.0">
		<link href="https://fonts.googleapis.com/css?family=PT+Sans:400,400i,700,700i|Roboto" rel="stylesheet">  
		<link rel="stylesheet" href="../css/fontello.css">
		<link rel="stylesheet" href="../css/bootstrap.css">
		<link rel="stylesheet" href="../css/bootstrap.min.css">
		<link rel="stylesheet" href="../css/estilos2.css">
		<title>Venta exitosa a usuario</title>
	</head>
		<body>
			<main>
				<div class="register-success">
					<div class="container">
						<div class="row">						 							
						 	<div class="titulo">¡Bacan! Haz concretado tu venta</div>
							<p class="texto">Haz hecho una venta a un miembro PymCash y lo haz premiado por su fidelidad, ¡Continúa enviando tus promociones para atraer más clientes, es un buen día para seguir aumentando tus ventas!. Tu venta ha quedado registrada en tus movimientos para tu control</p>
							<div class="boton">
								<button onclick="window.location.href='{{ route('home') }}'"><a href="#">Ir al inicio</a></button>
							</div>

						</div>
					</div>
				</div>
			</main>
			<script src="../js/jquery-3.3.1.min.js"></script>
			<script src="../js/menus.js"></script>
			<script src="../js/popper.min.js"></script>
			<script src="../js/boostrap.min.js"></script>
		</body>
</html>