@extends('layout_comercio_2')

@section('content')
<div class="header-billetera-comercio">
		<div class="icono-izquierda">
			<a href="{{ route('home') }}" class="icono-izquierda mt-2"><i class="icon-left-open"></i></a>
		</div>
		<div class="saldo"><p>Mis</p></div>
		<div class="imagen"><img src="../img/logopymcash2.png" alt="Logo PymCash"></div>
		<div class="monto" data-count="{{ $user->getBilletera() }}" >0</div>
		<div class="imagen2"><img src="../iconos/pymcashmoneda.png" alt="Moneda PymCash"></div>
	</div>

	<div class="main-billeteracomercio">
		<div class="boton-movimientos">
			<a href="{{ route('mis_movimientos') }}" class="btn"><img src="../iconos/mismovimientosblanco.png" alt=""> Mis Movimientos</a>
		</div>
	</div>
@endsection