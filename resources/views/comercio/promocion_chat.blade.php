@extends('layout_comercio')

@section('content')
<div class="main-promo">
		
		<div class="titulo">
			<p>Lista de Miembros</p>
		</div>
		
		    <form class="contenedor" method="post" action="{{ route('promocion_chat_enviar') }}">
			@csrf
		        <div class="group-checkall">
		        	<div class="texto">
		        		<p>Seleccionar todos</p>
		        	</div>
		        	<div class="input-checkall">
		        		<input type="checkbox" onClick="toggle(this)" value="">
		        	</div>
		        </div>
				<div class="group-checkbox" id="contactos" name="contactos">
					<!-- Contacto #1 -->
					@foreach($miembros as $miembro)
					<?php  
							$charset='ISO-8859-1'; // o 'UTF-8'
							$str = iconv($charset, 'ASCII//TRANSLIT',lcfirst($miembro->nombre[0]));
							$icon = preg_replace("/[^A-Za-z0-9 ]/", '', $str);
					?>
						<div class="contacto">
							<div class="imagen">
								<img src="../iconos/letras/{{ $icon }}.png"> 
							</div>
							<div class="nombre">
								<p>{{ $miembro->nombre }} {{ $miembro->apellido }}</p>
							</div>
							<div class="input-single">
								<input type="checkbox" name="checklist[]" value="{{ $miembro->rut }}">
							</div>
						</div>		          
					@endforeach
				</div>

		        <div class="mensaje">
		        	<div class="textarea">
		            	<textarea  id="msg" name="msg" placeholder="Escriba su promoción"></textarea>
		        	</div>           
		            <div class="boton">
		            	<button type="submit" class="btn button">Enviar promoción</button>
		            </div>
		        </div>
	   		</form>
	</div>

    <script language="JavaScript">
       function toggle(source) {
       checkboxes = document.getElementsByName('checklist[]');
       for(var i=0, n=checkboxes.length;i<n;i++){
       checkboxes[i].checked = source.checked;}}
    </script>

@endsection