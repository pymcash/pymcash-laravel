@extends('layout2')
@section('titulo')
    <title>Recuperar contraseña miembro PymCash</title>
@endsection
@section('content')
<main>
    <div class="recoverpass">
        <div class="container">
            <div class="row info">
                <p class="h2 titulo col-12">¿Olvidaste tu contraseña?</p>
                <div class="texto col-12">
                    <p>Ingresa tu rut y enviaremos un email al correo asociado</p>
                </div>
        
                <div class="form-group col-12">
                    <input type="text" name="rut" id="rut" placeholder="RUT" required="required">
                    </div>
                <div class="boton-sendmail col-12">
                    <button type="submit" name="submit" required="required">
                    Enviar Correo
                    </button>
                </div>
            </div>
        </div>
    </div>
</main>
@endsection