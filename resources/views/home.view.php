<?php require 'header.php'; ?>
		<div class="main">
			<section class="usuario">
				<div class="contenedor">
					<!-- Tuerca en pantalla grande -->
					<div class="d-none d-md-block tuerca">
						<a href="#" id="tuerca"><img src="../iconos/tuercablanca.png" width="22" height="22"></a>
					</div>

					<div class="col-md-3 barra-lateral-izquierda" id="barra-lateral-izquierda">
						<nav>
							<a href="#"><i class="icon-user"></i>Ficha Personal</a>
							<a href="#"><i class="icon-switch"></i>Cambiar Contraseña</a>
							<hr>
							<a href="#">Términos y condiciones</a>
							<a href="#">Cerrar Sesión</a>
						</nav>
					</div> 

					<a href="#" class="fondo-enlace" id="fondo-enlace"></a>

					<!-- Dinero en cuenta usuario -->
					<button type="button" class="btn boton-verde">
						<p class="cuenta">$5000</p>
						<p class="texto">Ver mi cuenta</p>
					</button>
					<!-- Foto usuario -->
					<div class="foto">
						<img src="../img/gustavo.jpg" width="125" alt="usuario">
					</div>
					<div class="texto">
						<h3 class="nombre">Gustavo Domínguez</h3>
						<p class="numero">Miembro #121</p>
					</div>
				</div>
			</section>

			<section class="menu-principal">
				<div class="container">
					<div class="row">
						<div class="col-md-3 col-sm-6 col-6">
							<button class="btn btn1">
								<a href="#"><img src="../iconos/micartera.png" alt=""></a>
							</button>
							<p class="titulo1">Mi Billetera</p>
						</div>
						<div class="col-md-3 col-sm-6 col-6">
							<button class="btn btn2">
								<a href="#"><img src="../iconos/mismovimientos.png" alt=""></a>
							</button>
							<p class="titulo2">Mis Movimientos</p>
						</div>
						<div class="col-md-3 col-sm-6 col-6">
							<button class="btn btn3">
								<a href="#"><img src="../iconos/misdatos.png" alt=""></a>
							</button>
							<p class="titulo3">Mis Datos</p>
						</div>
						<div class="col-md-3 col-sm-6 col-6">
							<button class="btn btn4">
								<a href="#"><img src="../iconos/invitefriends.png" alt=""></a>
							</button>
							<p class="titulo4">Invitar Amigos</p>
						</div>
					</div>
				</div>
			</section>
		</div>
<?php require 'footer.php'; ?>