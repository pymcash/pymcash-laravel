<?php require 'header.php'; ?>
	<div class="main-notificaciones">
		<div class="container">
			<div class="row">
				<div class="botones d-flex justify-content-center col-12 col-md-10 mt-3 mb-4">
					<div class="btn-group" role="group" aria-label="Basic example">
				  		<button type="button" class="btn btn-secondary izquierda">
				  			<a href="#">Notificaciones</a>
				  		</button>
				  		<button type="button" class="btn btn-secondary centro">
				  			<a href="{{ route('mensajeria') }}">Busqueda</a>
				  		</button>
				  		<button type="button" class="btn btn-secondary derecha activo">
				  			<a href="{{ route('chat_lista') }}">Chats</a>
				  		</button>
					</div>
				</div>
			</div>
		</div>
		
			<div class="container">
				<div class="row notificaciones">

					<!-- Notificacion #1 -->
					<div class="notificacion d-flex justify-content-center col-12 col-md-12">
						<div class="col-2 col-md-2 imagen">
							<img src="../iconos/letras/b.png">
						</div>
						<div class="col-8 col-md-6 content">
							<p class="titulo">Botillería La Lucy</p>
							<p class="texto">¡Oferta en Cerveza Escudo por el día de hoy!</p>
							<button class="btn btn-secondary d-block d-md-none">
								<a href="#">Abrir chat</a>
							</button>
						</div>
						<div class="col-md-2 d-none d-md-block">
							<a href="#" class="btn btn-secondary">Abrir chat</a>
						</div>
						<div class="col-2 col-md-2 iconos">
							<a href="#" class="close"><img src="../iconos/close.png"></a>
							<span class="badge badge-danger bluealert">1</span>
							<p class="datetime">12:30</p>
						</div>
					</div>
						<!-- Notificacion #2 -->
					<div class="notificacion d-flex justify-content-center col-12 col-md-12">
						<div class="col-2 col-md-2 imagen">
							<img src="../iconos/letras/b.png">
						</div>
						<div class="col-8 col-md-6 content">
							<p class="titulo">Botillería La Lucy</p>
							<p class="texto">¡Oferta en Cerveza Escudo por el día de hoy!</p>
							<button class="btn btn-secondary d-block d-md-none">
								<a href="#">Abrir chat</a>
							</button>
						</div>
						<div class="col-md-2 d-none d-md-block">
							<a href="#" class="btn btn-secondary">Abrir chat</a>
						</div>
						<div class="col-2 col-md-2 iconos">
							<a href="#" class="close"><img src="../iconos/close.png"></a>
							<span class="badge badge-danger bluealert">1</span>
							<p class="datetime">12:30</p>
						</div>
					</div>
					<!-- Notificacion #3 -->
					<div class="notificacion d-flex justify-content-center col-12 col-md-12">
						<div class="col-2 col-md-2 imagen">
							<img src="../iconos/letras/b.png">
						</div>
						<div class="col-8 col-md-6 content">
							<p class="titulo">Botillería La Lucy</p>
							<p class="texto">¡Oferta en Cerveza Escudo por el día de hoy!</p>
							<button class="btn btn-secondary d-block d-md-none">
								<a href="#">Abrir chat</a>
							</button>
						</div>
						<div class="col-md-2 d-none d-md-block">
							<a href="#" class="btn btn-secondary">Abrir chat</a>
						</div>
						<div class="col-2 col-md-2 iconos">
							<a href="#" class="close"><img src="../iconos/close.png"></a>
							<span class="badge badge-danger bluealert">1</span>
							<p class="datetime">12:30</p>
						</div>
					</div>
					<!-- Notificacion #4 -->
					<div class="notificacion d-flex justify-content-center col-12 col-md-12">
						<div class="col-2 col-md-2 imagen">
							<img src="../iconos/letras/b.png">
						</div>
						<div class="col-8 col-md-6 content">
							<p class="titulo">Botillería La Lucy</p>
							<p class="texto">¡Oferta en Cerveza Escudo por el día de hoy!</p>
							<button class="btn btn-secondary d-block d-md-none">
								<a href="#">Abrir chat</a>
							</button>
						</div>
						<div class="col-md-2 d-none d-md-block">
							<a href="#" class="btn btn-secondary">Abrir chat</a>
						</div>
						<div class="col-2 col-md-2 iconos">
							<a href="#" class="close"><img src="../iconos/close.png"></a>
							<span class="badge badge-danger bluealert">1</span>
							<p class="datetime">12:30</p>
						</div>
					</div>

				</div>
			</div>
		</div>
<?php require 'footer.php'; ?>