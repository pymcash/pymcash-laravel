<?php require 'head2.php'; ?>
	<header>
		<div class="header-movimientos">
			<div class="container">
				<div class="row barra col-12">
					<div class="icono-izquierda col-md-4 col-2">
						<a href="#"><i class="icon-left-open"></i></a>
					</div>
					<div class="titulo col-md-8 col-10">
						Mis Movimientos
					</div>
				</div>
			</div>
		</div>
	</header>
	<div class="main-movimientos">
		<div class="container">
			<div class="separador"></div>
			<div class="row tabla">
				<table class="table table-striped">
				  <thead class="bordes">
				    <tr class="cabecera">
				      <th scope="col">Fecha</th>
				      <th scope="col">Monto</th>
				      <th scope="col">Detalle</th>
				      <th scope="col">Cantidad PC</th>
				      <th scope="col">N° Boleta</th>
				    </tr>
				  </thead>
				  <tbody>
				    <tr class="table-borderless">
				      <th class="fecha" scope="row">16-04-2018 <br> 11:44AM</th>
				      <td class="monto">2000$</td>
				      <td class="detalle">Venta Cliente</td>
				      <td class="cuenta">-1 PCash</td>
				      <td class="boleta">5524462</td>
				    </tr>
				    <tr>
				      <th class="fecha" scope="row">17-04-2018 <br> 10:20AM</th>
				      <td class="monto">$2000</td>
				      <td class="detalle">Compra PymCash</td>
				      <td class="cuenta">+10 PCash</td>
				      <td class="boleta"></td>
				    </tr>
				    <tr>
				      <th class="fecha" scope="row">16-04-2018 <br> 12:30AM</th>
				      <td class="monto">2000$</td>
				      <td class="detalle">Venta Cliente</td>
				      <td class="cuenta">-1 PCash</td>
				      <td class="boleta">5524468</td>
				    </tr>
				   	<tr>
				      <th class="fecha" scope="row">20-04-2018 <br> 2:20AM</th>
				      <td class="monto">$2000</td>
				      <td class="detalle">Compra Pymcash</td>
				      <td class="cuenta">+10 PCash</td>
				      <td class="boleta"></td>
				    </tr>
				  </tbody>
				</table>
			</div>
			<div class="saldo">
				<p><img src="../iconos/PC-01.png" alt=""> Saldo: 50 </p>
			</div>
		</div>
	</div>
<?php require 'footer2.php'; ?>