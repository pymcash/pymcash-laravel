@extends('layout2')

@section('titulo')
    <title>Inicio PymCash</title>
@endsection
@section('content')
<main>
    <div class="chooselogin">
        <div class="container">
            <div class="row">
                <div class="titulo col-12">
                    <p class="h1">¿Ya tienes cuenta?</p>
                </div>
                <div class="texto col-12">
                    <p>Escoje como deseas iniciar sesión</p>
                </div>
                <div class="boton boton1 col-12">
                    <button onclick="window.location.href='{{ route('iniciar_sesion_usuario') }}'"><a href="{{ route('iniciar_sesion_usuario') }}">Soy Miembro PymCash</a></button>
                </div>
                <div class="boton col-12">
                    <button onclick="window.location.href='{{ route('iniciar_sesion_comercio') }}'"><a href="{{ route('iniciar_sesion_comercio') }}">Soy Comercio PymCash</a></button>
                </div>
                <div class="link col-12">
                    <a href="#">¿Aún no tienes cuenta? <strong>Ingresa aquí</strong></a>
                </div>
            </div>
        </div>
    </div>
</main>
@endsection