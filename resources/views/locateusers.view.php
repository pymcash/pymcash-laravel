<?php require 'head2.php'; ?>
<?php require 'header2.php'; ?>
	<div class="main-locateusers">
    
     <div id="map"></div>            
      <script>
      // Note: This example requires that you consent to location sharing when
      // prompted by your browser. If you see the error "The Geolocation service
      // failed.", it means you probably did not give permission for the browser to
      // locate you.
      var map, infoWindow;
      var markers = [];
      var marker;
      var defpos = {lat: -33.523524, lng: -70.734431};

      function setMarkerPos(){
        marker.setPosition(pos);
      }
      function initMap() {
        map = new google.maps.Map(document.getElementById('map'), {
          center: {lat: -33.523524, lng: -70.734431},
          zoom: 14,
          gestureHandling: 'greedy'
        });

        // This event listener will call addMarker() when the map is clicked.
        map.addListener('click', function(event) {
          //addMarker(event.latLng);
        });
        // Adds a marker at the center of the map.
        //addMarker(haightAshbury);


        infoWindow = new google.maps.InfoWindow;

        // Try HTML5 geolocation.
        if (navigator.geolocation) {
          navigator.geolocation.getCurrentPosition(function(position) {
            var pos = {
              lat: position.coords.latitude,
              lng: position.coords.longitude
            };

            infoWindow.setPosition(pos);
            infoWindow.setContent('Mi Posicion Actual');
            infoWindow.open(map);
          
            map.setCenter(pos);
            map.setZoom(15);
          }, function() {
            handleLocationError(true, infoWindow, map.getCenter());
          });
        } else {
          // Browser doesn't support Geolocation
          handleLocationError(false, infoWindow, map.getCenter());
        }
        //alert(getMarkerLatitud());


          function downloadUrl(url,callback) {
          var request = window.ActiveXObject ?
              new ActiveXObject('Microsoft.XMLHTTP') :
              new XMLHttpRequest;

          request.onreadystatechange = function() {
            if (request.readyState == 4) {
              request.onreadystatechange = doNothing;
              callback(request, request.status);
            }
          };

          request.open('GET', url, true);
          request.send(null);
          }


  // Change this depending on the name of your PHP or XML file
  downloadUrl('consumidoresmarkers.php', function(data) {
            var xml = data.responseXML;
            var markers = xml.documentElement.getElementsByTagName('marker');
            Array.prototype.forEach.call(markers, function(markerElem) {
              var name = markerElem.getAttribute('name');
              var address = markerElem.getAttribute(''); // No showing direction of member
              var point = new google.maps.LatLng(
                  parseFloat(markerElem.getAttribute('lat')),
                  parseFloat(markerElem.getAttribute('lng')));

              var infowincontent = document.createElement('div');
              var strong = document.createElement('strong');
              strong.textContent = name
              infowincontent.appendChild(strong);
              infowincontent.appendChild(document.createElement('br'));

              var text = document.createElement('text');
              text.textContent = address
              infowincontent.appendChild(text);
              var iconBase = "img/";
              var icono = iconBase + 'person.png'
              var marker = new google.maps.Marker({
                map: map,
                position: point,
                icon: icono
              });
              marker.addListener('click', function() {
                infoWindow.setContent(infowincontent);
                infoWindow.open(map, marker);
              });
            });
          });
        }

        
     
        function doNothing() {}

      function handleLocationError(browserHasGeolocation, infoWindow, pos) {
        infoWindow.setPosition(pos);
        infoWindow.setContent(browserHasGeolocation ?
                              'Error: The Geolocation service failed.' :
                              'Error: Your browser doesn\'t support geolocation.');
        infoWindow.open(map);
      }

      function toggleBounce() {
        if (marker.getAnimation() !== null) {
          marker.setAnimation(null);
        } else {
          marker.setAnimation(google.maps.Animation.BOUNCE);
        }
      }

      function UpdatePos(){
        document.getElementById("latitud").value = getMarkerLatitud();
        document.getElementById("longitud").value = getMarkerLongitud();
      }

      // Adds a marker to the map and push to the array.
      function addMarker(location,titulo) {
        var iconBase = 'img/';
        var marker = new google.maps.Marker({
          position: location,
          icon: iconBase + 'comercio.png',

          map: map
        });
        marker.setTitle(titulo);
        markers.push(marker);
      }

      // Sets the map on all markers in the array.
      function setMapOnAll(map) {
        for (var i = 0; i < markers.length; i++) {
          markers[i].setMap(map);
        }
      }

      // Removes the markers from the map, but keeps them in the array.
      function clearMarkers() {
        setMapOnAll(null);
      }

      // Shows any markers currently in the array.
      function showMarkers() {
        setMapOnAll(map);
      }

      // Deletes all markers in the array by removing references to them.
      function deleteMarkers() {
        clearMarkers();
        markers = [];
      }

      function getMarkerLatitud(){
        return marker.getPosition().lat()
      }
      function getMarkerLongitud(){
        return marker.getPosition().lng();
      }
      function AddMyMarker(xlat,xlong,titulo){
        addMarker({lat: xlat, lng: xlong},titulo);
      }
      function supermarker(){
        var l1,l2;
        l1 = document.getElementById("lat").value;
        l2 = document.getElementById("long").value;

        AddMyMarker(parseFloat(l1),parseFloat(l2),"");

      }


   </script>
    <script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCgaWQCCCIBx6I84EJLpH06C0fhyqq-I5E&callback=initMap">
    </script>

       
	</div>
<?php require 'footer2.php'; ?>