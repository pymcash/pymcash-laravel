@extends('layout2')

@section('titulo')

@endsection

@section('content')
<main>
<form id="form_id" action="{{ route('login') }}" method="POST">
@csrf
    <div class="user-commerce">
        <div class="container">
            <div class="row">
                <div class="imagen col-12">
                    <img src="../img/logopymcash.png" alt="Logotipo PymCash">
                </div>
                <div class="titulo col-12">
                    <p class="h3">¡Bienvenido!</p>
                </div>

                <div class="form-group col-12">
                    <i class="icon-user"></i><input class="inputrut" type="text" placeholder="RUT de registro" name="rut" id="rut" value="{{ old('rut') }}" required="required">

                </div>

                <div class="form-group col-12">
                    <i class="icon-lock"></i><input class="inputpassword" type="password" name="password" id="password" required="required" placeholder="Contraseña">
                </div>
                <label class="form-group col-12 remember">
                    <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Mantener sesión iniciada
                </label>
                @if ($errors->has('rut'))
                    <br>
                    <div class="form-group col-12">
                        <span>
                            <strong>{{ $errors->first('rut') }}</strong>
                        </span>
                    </div>
                @endif

                @if ($errors->has('password'))
                    <br>
                    <div class="form-group col-12">                
                        <span class="invalid-feedback">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                    </div>
                @endif


                <div class="form-group col-12">
                    <div class="boton login-button">
                        <button type="submit" name="submit" class="boton">
                            Entrar
                        </button>
                    </div>
                 

                    <div class="link col-12">
                        <a href="{{ route('recuperar_password') }}">¿Olvidaste tu contraseña? <strong>Ingresa aquí</strong></a>
                    </div>
                </div>

            </div>
        </div>
    </div>
</form>    
</main>
@endsection