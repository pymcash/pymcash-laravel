@extends('layout2')

@section('titulo')
<title>Comercio PymCash</title>
@endsection

@section('content')
<main>
    <div class="user-commerce">
        <div class="container">
            <div class="row">
                <div class="imagen col-12">
                    <img src="../img/logopymcash.png" alt="Logotipo PymCash">
                </div>
                <div class="titulo col-12">
                    <p class="h3">Comercio</p>
                </div>

                <div class="form-group col-12">
                    <i class="icon-user"></i><input class="inputrut" type="text" placeholder="RUT Comercial" name="user" id="user" required="required">
                </div>

                <div class="form-group col-12">
                    <i class="icon-lock"></i><input class="inputpassword" type="password" name="pass" id="pass" required="required" placeholder="Contraseña">
                </div>

                <div class="form-group col-12">
                    <div class="boton login-button">
                        <button type="submit" name="submit" class="boton">
                            Entrar
                        </button>
                    </div>
                    <div class="link col-12">
                        <a href="#">¿Olvidaste tu contraseña? <strong>Ingresa aquí</strong></a>
                    </div>
                </div>

            </div>
        </div>
    </div>
</main>
@endsection