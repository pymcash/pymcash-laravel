@extends('layout_adminpanel')

@section('content')
	<main class="main col">
		<div class="row">
			<div class="columna col-lg-12">
				<div class="container edit">
				<div class="titulo">
					<h3>Editar Usuario</h3>
				</div>
		 		<section class="miembros">
		 			<table>
						<thead>
							<tr class="table100-head">
								<th class="column1">N° Miembro</th>
								<th class="column2">RUT</th>
								<th class="column3">Nombre</th>
								<th class="column4">Apellido</th>
								<th class="column4">Teléfono</th>
								<th class="column5">Direccion</th>
								<th class="column6">Comuna</th>
								<th class="column7">Email</th>
							</tr>
						</thead>
						
						<tbody>
							<form method="post" action="">
			 					<tr class="busqueda">
			 						<td>#1</td> 
						 			<td><input type="text" name="rut" value="26135619-8"></td>
									<td><input type="text" name="nombre" value="Gustavo"></td>
									<td><input type="text" name="apellido" value="Dominguez"></td>
									<td><input type="text" name="telefono" value="+56 9 49975027"></td>
									<td><input type="text" name="direccion" value="Av Salesianos"></td>
									<td><input type="text" name="comuna" value="San Miguel"></td>
									<td><input type="text" name="email" value="gustavodominguez230@gmail.com"></td>
			 					</tr>
								<td><input type="submit" class="btn btn-aqua" value="Guardar Cambios"></td>
							</form>
						</tbody>
					</table>
		 		</section>
				</div>
			</div>
		</div>
	</main>
@endsection