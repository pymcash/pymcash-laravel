@extends('layout_adminpanel')

@section('content')
	<main class="main col">
		<div class="row">
			<div class="columna col-lg-12">
				<div class="container edit">
				<div class="titulo">
					<h3>Editar datos comercio</h3>
				</div>
		 		<section class="miembros">
		 			<table>
						<thead>
							<tr class="table100-head">
								<th class="column1">Rut Comercio</th>
								<th class="column2">Nombre del comercio</th>
								<th class="column3">Nombre de fantasía</th>
								<th class="column4">Teléfono</th>
								<th class="column5">Dirección</th>
								<th class="column6">Comuna</th>
								<th class="column7">Email</th>
							</tr>
						</thead>
						
						<tbody>
							<form method="post" action="">
			 					<tr class="busqueda">
						 			<td><input type="text" name="rut_comercio" value="77135619-8"></td>
									<td><input type="text" name="nombre_comercio" value="La Iguana Spa"></td>
									<td><input type="text" name="nombre_fantasia" value="Minimarket La Iguana"></td>
									<td><input type="text" name="telefono" value="+56 9 4344227"></td>
									<td><input type="text" name="direccion" value="Av Salesianos"></td>
									<td><input type="text" name="comuna" value="Maipu"></td>
									<td><input type="text" name="email" value="gustavodominguez230@gmail.com"></td>
			 					</tr>
								<td><input type="submit" class="btn btn-aqua" value="Guardar Cambios"></td>
							</form>
						</tbody>
					</table>
		 		</section>
				</div>
			</div>
		</div>
	</main>
@endsection