@extends('layout_adminpanel')

@section('content')
	<main class="main col">
		<div class="row">
			<div class="columna col-lg-12">
				<div class="container">
					<section class="buscador">
		 			<table>
		 				<thead>
		 					<tr>
		 						<!-- <th>Total miembros: 1800</th> -->
		 						<th>
									<form action="" method="post">
		 								<input class="text-box" type="text" name="rut_comercio" placeholder="Busqueda por rut:">
		 								<input class="btn btn-default" type="submit" value="Buscar">
		 							</form>
                                 </th>
                                <th>
                                    <form action="" method="post">
		 								<input class="text-box" type="text" name="nombre_fantasia" placeholder="Nombre Fantasía: ">
		 								<input class="btn btn-default" type="submit" value="Buscar">
		 							</form>
                                </th>
                                <th>
                                	<form action="" method="post">
		 								<input class="text-box" type="text" name="nombre_comercio"  placeholder="Nombre Comercio: ">
		 								<input class="btn btn-default" type="submit" value="Buscar">
		 							</form>
		 						</th>
		 						<th>
                                    <form action="" method="post">
		 								<input class="text-box" type="text" name="email"  placeholder="Busqueda por EMAIL: ">
		 								<input class="btn btn-default" type="submit" value="Buscar">
		 							</form>
		 						</th>
		 					</tr>
		 				</thead>
		 			</table>
		 			<div class="separador">
                  
		 			</div>
		 			</section>
		 		<section class="miembros">
		 			<table>
						<thead>
							<tr class="table100-head">
								<th class="column1">Rut Comercio</th>
								<th class="column2">Nombre del comercio</th>
								<th class="column3">Nombre de fantasía</th>
								<th class="column4">Direccion</th>
								<th class="column5">Teléfono</th>
								<th class="column6">Email</th>
								<th class="column7">Última conexión</th>
								<th class="column8"></th>
							</tr>
						</thead>
						
						<tbody>
		 					<tr class="busqueda"> 
				 				<td class="column1">77135619-8</td>
								<td class="column2">La Iguana Spa</td>
								<td class="column3">Minimarket La Iguana</td>
								<td class="column4">Maipu</td>
								<td class="column5">+56 9 4344227</td>
								<td class="column6">laiguana@gmail.com</td>
								<td class="column7">18-07-2018</td>
								<td class="column8"><a href="#"><i class="icon-edit"></i></a></td>
		 					</tr>
		 					<tr class="busqueda"> 
				 				<td class="column1">78135619-8</td>
								<td class="column2">Importaciones Sanchez</td>
								<td class="column3">Ferreteria Sanchez</td>
								<td class="column4">Providencia</td>
								<td class="column5">+56 9 4344427</td>
								<td class="column6">losandes@gmail.com</td>
								<td class="column7">18-07-2018</td>
								<td class="column8"><a href="#"><i class="icon-edit"></i></a></td>
		 					</tr>
						</tbody>
					</table>
		 		</section>
				</div>
			</div>
		</div>
	</main>
@endsection