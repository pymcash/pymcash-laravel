@extends('layout_adminpanel')

@section('content')
	<main class="main col">
		<div class="row">
			<div class="columna col-lg-12">
				<div class="container">
					<section class="buscador">
		 			<table>
		 				<thead>
		 					<tr>
		 						<!-- <th>Total miembros: 1800</th> -->
		 						<th>
									<form action="" method="post">
		 								<input class="text-box" type="text" name="rut" placeholder="Busqueda por rut:">
		 								<input class="btn btn-default" type="submit" value="Buscar">
		 							</form>
                                 </th>
                                <th>
                                    <form action="" method="post">
		 								<input class="text-box" type="number" name="id" placeholder="Busqueda por id: ">
		 								<input class="btn btn-default" type="submit" value="Buscar">
		 							</form>
                                </th>
                                <th>
                                	<form action="" method="post">
		 								<input class="text-box" type="text" name="nombre"  placeholder="Busqueda por nombre: ">
		 								<input class="btn btn-default" type="submit" value="Buscar">
		 							</form>
		 						</th>
		 						<th>
                                    <form action="" method="post">
		 								<input class="text-box" type="text" name="apellido"  placeholder="Busqueda por apellido: ">
		 								<input class="btn btn-default" type="submit" value="Buscar">
		 							</form>
		 						</th>
		 					</tr>
		 				</thead>
		 			</table>
		 			<div class="separador">
                  
		 			</div>
		 			</section>
		 		<section class="miembros">
		 			<table>
						<thead>
							<tr class="table100-head">
								<th class="column1">N° Miembro</th>
								<th class="column2">RUT</th>
								<th class="column3">Nombre</th>
								<th class="column4">Teléfono</th>
								<th class="column5">Comuna</th>
								<th class="column6">Email</th>
								<th class="column7">Última conexión</th>
								<th class="column8"></th>
							</tr>
						</thead>
						
						<tbody>
		 					<tr class="busqueda"> 
				 				<td class="column1">#1</td>
								<td class="column2">26135619-8</td>
								<td class="column3">Gustavo Dominguez</td>
								<td class="column4">+56 9 49975027</td>
								<td class="column5">San Miguel</td>
								<td class="column6">gustavodominguez230@gmail.com</td>
								<td class="column7">18-07-2018</td>
								<td class="column8"><a href="#"><i class="icon-edit"></i></a></td>
		 					</tr>
		 					<tr class="busqueda"> 
				 				<td class="column1">#1</td>
								<td class="column2">26135619-8</td>
								<td class="column3">Gustavo Dominguez</td>
								<td class="column4">+56 9 49975027</td>
								<td class="column5">San Miguel</td>
								<td class="column6">gustavodominguez230@gmail.com</td>
								<td class="column7">18-07-2018</td>
								<td class="column8"><a href="#"><i class="icon-edit"></i></a></td>
		 					</tr>
						</tbody>
					</table>
		 		</section>
				</div>
			</div>
		</div>
	</main>
@endsection