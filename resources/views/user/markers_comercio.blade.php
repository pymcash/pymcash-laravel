           
<markers>
@foreach($comercios as $comercio)
<marker 
        name = "{{ $comercio->getModel()->nombre_fantasia }}"
        address = "{{ $comercio->direccion }}"
        lat = "{{ $comercio->latitud }}"
        lng = "{{ $comercio->longitud }}"
        obj = "{{ $comercio->getModel()->razon_social }}"
/>
@endforeach
</markers>