<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width:device-width, user-scalable=no, initial-scale=1, maximum-scale=1, minimum-scale=1.0">
		<link href="https://fonts.googleapis.com/css?family=PT+Sans:400,400i,700,700i|Roboto" rel="stylesheet">  
		<link rel="stylesheet" href="../../css/fontello.css">
		<link rel="stylesheet" href="../../css/bootstrap.css">
		<link rel="stylesheet" href="../../css/bootstrap.min.css">
		<link rel="stylesheet" href="../../css/estilos2.css">
		<title>Compra exitosa en comercio</title>
	</head>
		<body>
			<main>
				<div class="register-success">
					<div class="container">
						<div class="row">						 							
						 	<div class="titulo">¡Felicidades!</div>
							<p class="texto">¡Enhorabuena! Haz recibido <strong>{{ $monto }}</strong> pesos por tu compra en el comercio <strong>{{ $comercio->nombre_fantasia }}</strong>. Sigue realizando tus compras en comercios aliados para ganar más lucas y canjees a partir de $1000</p>
							<div class="boton">
								<button onclick="window.location.href='{{ route('billetera') }}'"><a href="#">Ver billetera</a></button>
							</div>

						</div>
					</div>
				</div>
			</main>
			<script src="../../js/jquery-3.3.1.min.js"></script>
			<script src="../../js/menus.js"></script>
			<script src="../../js/popper.min.js"></script>
			<script src="../../js/bootstrap.min.js"></script>
		</body>
</html>
