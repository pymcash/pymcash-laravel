@extends('layout_user_2')

@section('content')
	<body>
		<div class="header">
			<div class="container">
				<div class="row chat-header">
					<div class="col-2 icono-izquierda">
						<a href="{{ route('chat_lista') }}" class="icono-izquierda mt-2"><i class="icon-left-open"></i></a>
					</div>
				    
					<?php  
							$charset='ISO-8859-1'; // o 'UTF-8'
							$str = iconv($charset, 'ASCII//TRANSLIT',lcfirst($comercio->nombre_fantasia[0]));
							$icon = preg_replace("/[^A-Za-z0-9 ]/", '', $str);
					?>
					<div class="col-2 imagen">
						<img src="../iconos/letras/{{ $icon }}.png">
					</div>
					<div class="col-6 titulo">
						<p>{{ $comercio->nombre_fantasia }}</p>

					</div>

					
					<div class="col-2 icono-derecha">
						<a href="#" id="tuercachat"><img src="../iconos/tuercagris.png" width="22" height="22"></a>
					</div>	

					<!-- Menu desplegable chat -->
				    <div class="col-md-3 barra-lateral-chat" id="barra-lateral-chat">
						<nav>
							<a href="{{ route('bloquear',['rut' => $comercio_rut ]) }}"><i class="icon-lock"></i>Bloquear Comercio</a>
							<hr>
							<a href="{{ route('vaciar_chat',['rut' => $comercio_rut ]) }}"><i class="icon-trash-empty"></i>Vaciar Chat</a>
						</nav>
					</div> 
					<!-- Fondo menu -->
					<a href="#" class="fondo-enlace-chat" id="fondo-enlace-chat"></a>
				</div>
			</div>
		</div>
		<div class="chat-history"  id="history-x" name="history-x">
			<div class="container">
				<div class="row" id="chat-history-x" name="chat-history-x" >
					<ul class="col-12" >

						@if ($mensajes!==NULL)
							@foreach($mensajes as $mensaje)
								@if ($user->rut == $mensaje->rut)
									<li class="clearfix col-12">
										<div class="message-data d-flex justify-content-end">
											<span class="message-data-time">{{ date('h:i:s A',strtotime($mensaje->time)) }}</span> &nbsp; &nbsp;
											<span class="message-data-name">{{ $nombre }}</span>
										</div>
										<div class="message other-message d-flex justify-content-end">
											{{ $mensaje->message }}
										</div>
									</li>								
								@else
									<li class="col-12">
										<div class="message-data">											
											<span class="message-data-name">{{ $model::where("rut","{$mensaje->rut}")->first()->getModel()->nombre_fantasia }}</span>
											<span class="message-data-time">{{ date('h:i:s A',strtotime($mensaje->time)) }}</span>
										</div>
										<div class="message my-message">
											{{ $mensaje->message }}
										</div>
									</li>
								@endif

							@endforeach

						@endif
						
					</ul>
				</div>
			</div>
		</div>
		<div class="chat-messages">
			<div class="container">
				<div class="row">
					<div class="col-12">
						<textarea name="message-to-send" id="message-to-send" placeholder=" Escriba su mensaje" rows="3"></textarea>
					</div>
					<div class="col-12 d-flex justify-content-end">
						<button>ENVIAR</button>
					</div>
				</div>
			</div>
		</div>



	</body>

@endsection

<script src="../js/jquery-3.3.1.min.js"></script>
<script src='../js/handlebars.min.js'></script>
<script src='../js/list.min.js'></script> 


<script>

$(document).ready(function () {



 var chat = {
    messageToSend: '',
	messageTemplate: [
		'<li class="clearfix col-12">',
		'<div class="message-data d-flex justify-content-end">',
		'<span class="message-data-time">',
		'',
		'</span> &nbsp; &nbsp;',
		'<span class="message-data-name">{{ $nombre }}',
		'</span>',
		'</div>',
		'<div class="message other-message d-flex justify-content-end">',
		'',
		'</div>',
		'</li>'
	],
	messageTemplate2: [
		'<li class="clearfix col-12">',
		'<div class="message-data">',
		'<span class="message-data-time">',
		'',
		'</span> &nbsp; &nbsp;',
		'<span class="message-data-name">{{ $nombre }}',
		'</span>',
		'</div>',
		'<div class="message my-message">',
		'',
		'</div>',
		'</li>'
	],
    init: function() {
      this.cacheDOM();
      this.bindEvents();
      this.render();
    },
    cacheDOM: function() {
      this.$chatHistory = $('#chat-history-x');
      this.$button = $('button');
      this.$textarea = $('#message-to-send');
      this.$chatHistoryList =  this.$chatHistory.find('ul');
    },
    bindEvents: function() {
      this.$button.on('click', this.addMessage.bind(this));
      this.$textarea.on('keyup', this.addMessageEnter.bind(this));
	},
	mi_mensaje: function (){
		var Template = this.messageTemplate;
		Template[3] = this.getCurrentTime();
		Template[9] = this.messageToSend;
		this.$chatHistoryList.append(Template.join(""));       
	},
	tu_mensaje: function ()
	{
		var Template = this.messageTemplate2;
		Template[3] = this.getCurrentTime();
		Template[9] = this.messageToSend;
		this.$chatHistoryList.append(Template.join(""));  
	},
    render: function() {
      this.scrollToBottom();
      if (this.messageToSend.trim() !== '') {


		var xhr = new XMLHttpRequest();
        xhr.open('POST',"{{ route('insertar',['rut' => $comercio_rut  ]) }}", true);
        xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
        xhr.onload = function () {
            // do something to response
            console.log(this.responseText);
        };
        xhr.send('msg='+this.messageToSend);

		
		this.mi_mensaje();        
		this.$textarea.val('');
		this.scrollToBottom();
		
		
		
      }
      
    },
    
    addMessage: function() {
      this.messageToSend = this.$textarea.val()
      this.render();         
    },
    addMessageEnter: function(event) {
        // enter was pressed
        if (event.keyCode === 13) {
          this.addMessage();
        }
    },
    scrollToBottom: function() {
		var $chat = $(".chat-history");
		$chat.scrollTop($chat[0].scrollHeight);
    },
    getCurrentTime: function() {
      return new Date().toLocaleTimeString().
              replace(/([\d]+:[\d]{2})(:[\d]{2})(.*)/, "$1$3");
    },
    getRandomItem: function(arr) {
      return arr[Math.floor(Math.random()*arr.length)];
    }
    
  };


chat.init();

  
});



</script>