@extends('layout_user')

@section('content')
<div class="main-notificaciones">
		<div class="container">
			<div class="row">
				<div class="botones d-flex justify-content-center col-12 col-md-10 mt-3">
					<div class="btn-group" role="group" aria-label="Basic example">
				  		<button type="button" class="btn btn-secondary izquierda activo">
                            <a href="{{ route('notificaciones') }}">Notificaciones
                                @if ($notify_count>0)  
                                    <span class="badge badge-danger bluealert">{{ $notify_count }}</span>
                                @endif
                            </a>
				  		</button>
				  		<button type="button" class="btn btn-secondary centro">
				  			<a href="{{ route('mensajeria') }}">Búsqueda</a>
				  		</button>
				  		<button type="button" class="btn btn-secondary derecha">
				  			<a href="{{ route('chat_lista') }}">Chats</a>
				  		</button>
					</div>
				</div>
				
				<div class="botones2 d-flex justify-content-end col-8-inverse col-md-2 mt-2">
					<div class="deleteall">
						<a href="{{ route('eliminar_notificaciones') }}">
							<img src="../iconos/eliminar.png" width="22" height="22">
							<p class="texto">Eliminar Todas</p>
						</a>
					</div>
				</div>
			</div>
		</div>
		
			<div class="container">
				<div class="row notificaciones">

                    @if (count($notificaciones))

                        @foreach($notificaciones as $notificacion)
                            <!-- Notificaciones -->
                            <div class="notificacion d-flex justify-content-center col-12 col-md-12">
                                <div class="col-2 col-md-2 imagen">
                                    <img src="../iconos/letras/n.png">
                                </div>
                                <div class="col-8 col-md-6 content">
                                    <p class="titulo">{{ $notificacion->titulo }}</p>
                                    <p class="texto">{{ $notificacion->mensaje }}</p>
                                </div>
                                <div class="col-md-2 d-none d-md-block">
                                    <a href="{{ route('ver_notificacion',['id' => $notificacion->id ]) }}" class="btn btn-secondary">Ver</a>
                                </div>
                                <div class="col-2 col-md-2 iconos">
                                    <a href="{{ route('eliminar_notificacion',['id' => $notificacion->id ]) }}" class="close"><img src="../iconos/close.png"></a>
                                    <br>
                                    <p class="datetime">{{ date('h:i:s A',strtotime($notificacion->time)) }}</p>
                                </div>
                            </div>

                        @endforeach

                    @endif



				</div>
			</div>
		</div>
@endsection


