@extends('layout_user_2')

@section('content')
<header>
		<div class="header-datauser">
			<div class="container">
				<div class="row barra col-12">
					<div class="icono-izquierda col-md-4 col-2">
						<a href="{{ route('home') }}"><i class="icon-left-open"></i></a>
					</div>
					<div class="titulo col-md-4 col-10">
						Datos Personales
					</div>
				</div>
			</div>
		</div>
	</header>
	<div class="main-datauser">
		<div class="container">
			<div class="row datos">
				
				<form id="main-contact-form col-12" class="contact-form" name="contact-form" method="post" action="{{ route('mis_datos') }}" enctype="multipart/form-data"> 
					<input type="hidden" name="_method" value="PUT">
    				<input type="hidden" name="_token" value="{{ csrf_token() }}">
	                 <div class="imagen col-12">
						<img src="../img/iconuser.png" id="DNI" width="200px" name="DNI" alt="Documento de identidad">
					</div>
                  	
                  	<div class="rut">
                        <input type="text" name="rut" id="rut" placeholder="RUT" value="{{ $user->rut }}" readonly>
                  	</div>
                    
                    <div class="nombre">
						<input type="text" name="nombre" id="nombre" placeholder="Nombre" value="{{ $user->getModel()->nombre }}" readonly>
                    </div>
					
					<div class="apellido">
				 		<input type="text" name="apellido" id="apellido" placeholder="Apellido" value="{{ $user->getModel()->apellido }}" readonly>
					</div>

					<div class="direccion">
						<input type="text" name="direccion" id="direccion" class="input100" required="required" value="{{ $user->direccion }}"  placeholder="Direccion">
					</div>
    
	                <div class="comuna">
	                 	<input type="text" name="comuna" id="comuna" class="input100" required="required" value="{{ $user->comuna }}" placeholder="Comuna">
	                </div>
                     
                     <div class="telefono">
                     	<input type="text" name="telefono" id="telefono" class="input100" required="required" value="{{ $user->telefono }}" placeholder="Teléfono">
                     </div>

					<div class="mail">
						
						@if ($errors->has('email'))
							<input type="text" name="email" id="email" class="input100" required="required" value="{{ old('email') }}" placeholder="Email">
							<div class="form-group text-center">                                                                        
								<strong style="color:#bd1f33" >{{ $errors->first('email') }}</strong>                                                                       
							</div>                                                
						@else
							<input type="text" name="email" id="email" class="input100" required="required" value="{{ $user->email }}" placeholder="Email">
                    	@endif     
					</div>
                     
                	<div class="boton">
                		<button type="submit" name="save" id="save">
						Guardar Cambios
						</button>
                	</div>
                 
				</form>


			</div>
		</div>
	</div>    
@endsection