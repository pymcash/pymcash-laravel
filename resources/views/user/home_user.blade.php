@extends('layout_user')

@section('content')
<div class="main">
			<section class="usuario">
				<div class="contenedor">
					<!-- Tuerca en pantalla grande -->
					<div class="d-none d-md-block tuerca">
						<a href="#" id="tuerca"><img src="../iconos/tuercablanca.png" width="22" height="22"></a>
					</div>

					<div class="col-md-3 barra-lateral-izquierda" id="barra-lateral-izquierda">
						<nav>
							<a href="{{ route('mis_datos') }}"><i class="icon-user"></i>Ficha Personal</a>
							<a href="{{ route('cambiar_contraseña') }}"><i class="icon-switch"></i>Cambiar Contraseña</a>
							<hr>
							<a href="#">Términos y condiciones</a>
							<a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Cerrar Sesión</a>
						</nav>
					</div> 

					<a href="#" class="fondo-enlace" id="fondo-enlace"></a>

					<!-- Dinero en cuenta usuario -->
					<button type="button" onclick="window.location.href='{{ route('billetera') }}'" class="btn boton-verde">
						<p class="cuenta" data-count="{{ $user->getBilletera() }}">$0</p>
						<p class="texto">Ver mi cuenta</p>
					</button>
					<!-- Foto usuario -->
					<div class="foto">
						<img src="../img/iconuser.png" width="125" height="125" alt="usuario">
					</div>
					<div class="texto">
						<h3 class="nombre">{{ $user->getModel()->nombre }} {{ $user->getModel()->apellido }} </h3>
						<p class="numero">Miembro #{{ $user->id }}</p>
					</div>
				</div>
			</section>

			<section class="menu-principal">
				<div class="container">
					<div class="row">
						<div class="col-md-3 col-sm-6 col-6">
							<button onclick="window.location.href='{{ route('billetera') }}'" class="btn btn1">
								<a href="{{ route('billetera') }}"><img src="../iconos/micartera.png" alt=""></a>
							</button>
							<p class="titulo1">Mi Billetera</p>
						</div>
						<div class="col-md-3 col-sm-6 col-6">
							<button onclick="window.location.href='{{ route('mis_movimientos') }}'" class="btn btn2">
								<a href="{{ route('mis_movimientos') }}"><img src="../iconos/mismovimientos.png" alt=""></a>
							</button>
							<p class="titulo2">Mis Movimientos</p>
						</div>
						<div class="col-md-3 col-sm-6 col-6">
							<button onclick="window.location.href='{{ route('mis_datos') }}'" class="btn btn3">
								<a href="{{ route('mis_datos') }}"><img src="../iconos/misdatos.png" alt=""></a>
							</button>
							<p class="titulo3">Mis Datos</p>
						</div>
						<div class="col-md-3 col-sm-6 col-6">
							<button onclick="window.location.href='{{ route('invitar_amigo') }}'" class="btn btn4">
								<a href="{{ route('invitar_amigo') }}"><img src="../iconos/invitefriends.png" alt=""></a>
							</button>
							<p class="titulo4">Invitar Amigos</p>
						</div>
					</div>
				</div>
			</section>
		</div>
@endsection