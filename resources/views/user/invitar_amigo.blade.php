@extends('layout_user')

@section('content')
<div class="main-invite">
		<div class="container">
			<div class="row info">
				<p class="h1 titulo col-12">¡Invita a más personas a unirse!</p>
				<p class="texto col-12">Ingresa el correo de las personas que deseas, para mas de un correo separe con coma<br>ej. ejemplo1@mail.com, ejemplo2@mail.com, ejemplo3@mail.com</p>

				<div class="form-group col-12">
					<input type="text" name="correo" id="correo" placeholder="Email" required="required">
               	 </div>
               	<div class="boton-invite col-12">
	               <button type="submit" name="submit" required="required">
					Enviar Invitacion
					</button>
               	</div>
			</div>
		</div>
	</div>
@endsection