@extends('layout_user')

@section('content')
<div class="main-mensajeria">
		<div class="container">
			<div class="row">
				<div class="botones d-flex justify-content-center col-12 col-md-10 mt-3 mb-4">
					<div class="btn-group" role="group" aria-label="Basic example">
				  		<button type="button" class="btn btn-secondary izquierda">
						  <a href="{{ route('notificaciones') }}">Notificaciones
                                @if ($notify_count>0)  
                                    <span class="badge badge-danger bluealert">{{ $notify_count }}</span>
                                @endif
                            </a>
				  		</button>
				  		<button type="button" class="btn btn-secondary centro activo">
				  			<a href="{{ route('mensajeria') }}">Busqueda</a>
				  		</button>
				  		<button type="button" class="btn btn-secondary derecha">
				  			<a href="{{ route('chat_lista') }}">Chats</a>
				  		</button>
					</div>
				</div>
			
			</div>
		</div>
		
		<div class="container">
			<div class="row barrabusqueda">
				 <div class="barra input-group col-md-12 d-flex justify-content-center">
	                <input type="text" id="busqueda" name="busqueda" class="form-control barra" placeholder="Buscar contacto" aria-describedby="basic-addon1">
	                <i class="icon-search"></i>
                </div>
			</div>
		</div>

		<br/>
		<div class="container">
			<div class="row contactos">
				
				@if (isset($comercios))
					@foreach($comercios as $comercio)
						
						<a href="{{ route('chat',['rut' => $comercio->rut ]) }}" class="contacto d-flex justify-content-center col-12 col-md-12">
						<div class="col-2 col-md-4 imagen">
						<?php  
							$charset='ISO-8859-1'; // o 'UTF-8'
							$str = iconv($charset, 'ASCII//TRANSLIT',lcfirst($comercio->nombre_fantasia[0]));
							$icon = preg_replace("/[^A-Za-z0-9 ]/", '', $str);
						?>
						
							<img src="../iconos/letras/{{ $icon }}.png">
						</div>
						<div class="col-10 col-md-8 content">
							<p class="titulo">{{ $comercio->nombre_fantasia }}</p>
							<p class="texto">{{ $comercio->razon_social }}</p>
						</div>
						</a>						
					@endforeach
				@endif

			</div>
		</div>

	</div>
@endsection
