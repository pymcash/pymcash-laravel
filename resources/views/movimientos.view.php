<?php require 'head.php'; ?>
	<header>
		<div class="header-movimientos">
			<div class="container">
				<div class="row barra col-12">
					<div class="icono-izquierda col-md-4 col-2">
						<a href="#"><i class="icon-left-open"></i></a>
					</div>
					<div class="titulo col-md-8 col-10">
						Mis Movimientos
					</div>
				</div>
			</div>
		</div>
	</header>
	<div class="main-movimientos">
		<div class="container">
			<div class="separador"></div>
			<div class="row tabla">
				<table class="table table-striped">
				  <thead class="bordes">
				    <tr class="cabecera">
				      <th scope="col">Fecha</th>
				      <th scope="col">Monto</th>
				      <th scope="col">Detalle</th>
				      <th scope="col">Nro. Cuenta</th>
				    </tr>
				  </thead>
				  <tbody>
				    <tr class="table-borderless">
				      <th class="fecha" scope="row">16-04-2018 <br> 11:44AM</th>
				      <td class="monto">200$</td>
				      <td class="detalle">Ingreso Ficha</td>
				      <td class="cuenta">0000-0000-0000</td>
				    </tr>
				    <tr>
				      <th class="fecha" scope="row">17-04-2018 <br> 10:20AM</th>
				      <td class="monto">$2000</td>
				      <td class="detalle">Canje de Saldo</td>
				      <td class="cuenta">0000-0000-0000</td>
				    </tr>
				    <tr>
				      <th class="fecha" scope="row">16-04-2018 <br> 12:30AM</th>
				      <td class="monto">50$</td>
				      <td class="detalle">Ingreso Ficha</td>
				      <td class="cuenta">0000-0000-0000</td>
				    </tr>
				   	<tr>
				      <th class="fecha" scope="row">20-04-2018 <br> 2:20AM</th>
				      <td class="monto">$1000</td>
				      <td class="detalle">Canje de Saldo</td>
				      <td class="cuenta">0000-0000-0000</td>
				    </tr>
				  </tbody>
				</table>
			</div>
		</div>
	</div>
<?php require 'footer.php'; ?>