<?php require 'header2.php'; ?>
	<div class="main-mensajeria">
		<div class="container">
			<div class="row">
				<div class="botones d-flex justify-content-center col-12 col-md-10 mt-3 mb-4">
					<div class="btn-group" role="group" aria-label="Basic example">
				  		<button type="button" class="btn btn-secondary izquierda">
				  			<a href="#">Notificaciones</a>
				  		</button>
				  		<button type="button" class="btn btn-secondary derecha activo">
				  			<a href="#">Búsqueda</a>
				  		</button>
				  		<button type="button" class="btn btn-secondary derecha">
				  			<a href="#">Chats</a>
				  		</button>
					</div>
				</div>
			</div>
		</div>
		
		<div class="container">
			<div class="row barrabusqueda">
				 <div class="barra input-group col-md-12 d-flex justify-content-center">
	                <input type="text" id="busqueda" name="busqueda" class="form-control barra" placeholder="Buscar contacto" aria-describedby="basic-addon1"
	                >
	                <i class="icon-search"></i>
                </div>
			</div>
		</div>

		<br/>
		<div class="container">
			<div class="row contactos">

				<!-- Contacto #1 -->
				<a href="#" class="contacto d-flex justify-content-center col-12 col-md-12">
					<div class="col-2 col-md-4 imagen">
						<img src="../iconos/letras/a.png">
					</div>
					<div class="col-10 col-md-8 content">
						<p class="titulo">Ambar Gimenez</p>
						<p class="texto"></p>
					</div>
				</a>

				<!-- Contacto #2 -->
				<a href="#" class="contacto d-flex justify-content-center col-12 col-md-12">
					<div class="col-2 col-md-4 imagen">
						<img src="../iconos/letras/b.png">
					</div>
					<div class="col-10 col-md-8 content">
						<p class="titulo">John Smith</p>
						<p class="texto">Necesito 1 six pack de escudo</p>
					</div>
				</a>

				<!-- Contacto #3 -->
				<a href="#" class="contacto d-flex justify-content-center col-12 col-md-12">
						<div class="col-2 col-md-4 imagen">
							<img src="../iconos/letras/b.png">
						</div>
						<div class="col-10 col-md-8 content">
							<p class="titulo">Gustavo Dominguez</p>
							<p class="texto">¿Tienen disponible harina pan?</p>
						</div>
				</a>

				<!-- Contacto #4 -->
				<a href="#" class="contacto d-flex justify-content-center col-12 col-md-12">
						<div class="col-2 col-md-4 imagen">
							<img src="../iconos/letras/b.png">
						</div>
						<div class="col-10 col-md-8 content">
							<p class="titulo">Oscar Farías</p>
							<p class="texto">¿Tiene disponible 12Pack de Corona?</p>
						</div>
				</a>

				<!-- Contacto #5 -->
				<a href="#" class="contacto d-flex justify-content-center col-12 col-md-12">
						<div class="col-2 col-md-4 imagen">
							<img src="../iconos/letras/b.png">
						</div>
						<div class="col-10 col-md-8 content">
							<p class="titulo">Rey Stimpy</p>
							<p class="texto">Tienen globos plateados?</p>
						</div>
				</a>

					<!-- Contacto #6 -->
				<a href="#" class="contacto d-flex justify-content-center col-12 col-md-12">
						<div class="col-2 col-md-4 imagen">
							<img src="../iconos/letras/z.png">
						</div>
						<div class="col-10 col-md-8 content">
							<p class="titulo">Zafiro Gomez</p>
							<p class="texto"></p>
						</div>
				</a>

			</div>
		</div>

	</div>
<?php require 'footer2.php'; ?>