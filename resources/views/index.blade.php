@extends('layout2')
@section('titulo')
    <title>Inicio PymCash</title>
@endsection

@section('content')
<main>
    <div class="container">
        <div class="row">
            <div class="imagen col-12">
                <img src="../img/logopymcash.png" alt="Logotipo PymCash">
            </div>
            <div class="texto col-12">
                <p>¡Regístrate Ahora!</p>
            </div>
            <div class="boton col-12">
                <button onclick="window.location.href='{{ route('crear_cuenta') }}'"><a href="{{ route('crear_cuenta') }}">Crear Cuenta</a></button>
            </div>
            <div class="link col-12">
                <a href="{{ route('iniciar_sesion') }}">¿Ya tienes cuenta? <strong>Ingresa aquí</strong></a>
            </div>
        </div>
    </div>
</main>
@endsection
