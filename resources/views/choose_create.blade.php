@extends('layout2')

@section('titulo')
    <title>Inicio PymCash</title>
@endsection
@section('content')
<main>
    <div class="chooselogin">
        <div class="container">
            <div class="row">
                <div class="titulo col-12">
                    <p class="h2">¿Aún no tienes cuenta?</p>
                </div>
                <div class="textocreate col-12">
                    <p>¡Cualquiera puede ser miembro de la comunidad!<br> Recuerda que para ser comercio debes tener un local comercial</p>
                </div>
                <div class="boton boton1 col-12">
                    <button onclick="window.location.href='{{ route('crear_usuario') }}'"><a href="{{ route('crear_usuario') }}">Deseo ser miembro</a></button>
                </div>
                <div class="boton col-12">
                    <button onclick="window.location.href='{{ route('crear_comercio') }}'"><a href="{{ route('crear_comercio') }}">Deseo ser comercio</a></button>
                </div>
                <div class="link col-12">
                    <a href="{{ route('iniciar_sesion') }}">¿Ya tienes cuenta? <strong>Ingresa aquí</strong></a>
                </div>
            </div>
        </div>
    </div>
</main>
@endsection
