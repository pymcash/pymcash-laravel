<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width:device-width, user-scalable=no, initial-scale=1, maximum-scale=1, minimum-scale=1.0">
		<link href="https://fonts.googleapis.com/css?family=PT+Sans:400,400i,700,700i|Roboto" rel="stylesheet">  
		<link rel="stylesheet" href="../css/fontello.css">
		<link rel="stylesheet" href="../css/bootstrap.css">
		<link rel="stylesheet" href="../css/bootstrap.min.css">
		<link rel="stylesheet" href="../css/estilos3.css">
		<title>Comercios PymCash</title>
	</head>
@yield('content')

		<footer>
		</footer>

		<script src="../js/jquery-3.3.1.min.js"></script>
		<script src="../js/menus2.js"></script>
		<script src="../js/popper.min.js"></script>
		<script src="../js/bootstrap.min.js"></script>
	</body>
</html>
<script>
$('.monto').each(function() {
  var $this = $(this),
      countTo = $this.attr('data-count');
  
  $({ countNum: $this.text()}).animate({
    countNum: countTo
  },

  {

    duration: 500,
    easing:'linear',
    step: function() {
	  $this.html('<p>$'+Math.floor(this.countNum)+'</p>');
    },
    complete: function() {
	  $this.html('<p>$'+this.countNum+'</p>');
    }

  });  
});
</script>