<!DOCTYPE html>
<html lang="es">
	<head>
		<title>PymCash Administración</title>
		<meta charset="UTF-8">
		<meta name="viewport" content="width:device-width, user-scalable=no, initial-scale=1, maximum-scale=1, minimum-scale=1.0">
		<link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">
		<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,700|Roboto:300,400,500" rel="stylesheet"> 
		<link rel="stylesheet" href="{{ asset('css/fontello.css') }}">
		<link rel="stylesheet" href="{{ asset('css/estilos4.css') }}">
	</head>
	<body>
		<div class="container-fluid">
			<div class="row">
				<div class="barra-lateral col-12 col-sm-auto">
					<div class="logo">
						<div class="imagen">
							<img src="{{ asset('/img/logopymcash2.png') }}" alt="Logo PymCash">
						</div>
					</div>
					<nav class="menu d-flex d-sm-block justify-content-center flex-wrap">
						<a href="{{ route('principaladmin') }}"><i class="icon-gauge"></i><span>Inicio</span></a>
						<a href="{{ route('adminusers') }}"><i class="icon-users"></i><span>Administrar Usuarios</span></a>
						<a href="{{ route('admincommerce') }}"><i class="icon-home"></i><span>Administrar Comercios</span></a>
						<a href="#"><i class="icon-cog-alt"></i><span>Configuraciones</span></a>
						<a href="#"><i class="icon-logout"></i><span>Salir</span></a>
					</nav>
				</div>
		@yield('content')