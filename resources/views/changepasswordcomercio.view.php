<?php require 'head2.php'; ?>
	<header>
		<div class="header-changepassword">
			<div class="container">
				<div class="row barra col-12">
					<div class="icono-izquierda col-md-4 col-2">
						<a href="#"><i class="icon-left-open"></i></a>
					</div>
					<div class="titulo col-md-8 col-10">
						Cambiar contraseña
					</div>
				</div>
			</div>
		</div>
	</header>
	<div class="divisor"></div>
	<div class="contenedor-changepassword">
		<form method="post" action="changepassword.php">

        	<div class="form-group">
				<input type="password" pattern=".{8,12}" required title="8 a 12 caracteres" name="pass" id="pass" required="required" placeholder="Nueva contraseña">
            </div> 

            <div class="form-group">
				<input type="password" pattern=".{8,12}" required title="8 a 12 caracteres" placeholder="Confirmar contraseña" name="passconfirm" id="passconfirm" required="required">
            </div>
                    
            <div class="form-group">
                <button type="submit" value="cambiarpass" id="cambiarpass" name="cambiarpass">
				Cambiar Contraseña
				</button>
			</div>
		</form>
	</div>
	
<?php require 'footer2.php'; ?>