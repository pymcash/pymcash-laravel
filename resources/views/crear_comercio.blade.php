@extends('layout2')

@section('titulo')
    <title>Registro comercio PymCash</title>
@endsection
@section('content')
<header>
    <div class="icono-izquierda">
        <a href="{{ route('crear_cuenta') }}" class="icono-izquierda mt-2"><i class="icon-left-open-big"></i></a>
    </div>
        <div class="texto1 h3 text-center">
            <p>¡Bienvenido!</p>
        </div>
</header>
<main>
    <div class="register-user-commerce">
        <div class="container">
            <div class="row col-12">
                
                <form id="main-contact-form" class="formulario col-12" name="contact-form" method="post" action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']); ?>" enctype="multipart/form-data" > 
                
                <input type='file' id="imgInp" name="imgInp" class="file" style="display:none">
                    <div class="text-center imagen">
                        <img id="blah" src="../img/icon-user.png" class="img-rounded" alt="Foto de perfil">
                    </div>

                <div class="form-group text-center boton">
                    <a onclick="document.getElementById('imgInp').click();" class="login1" role="button">Cargar Foto</a>		
                </div>
                
                <div class="form-group text-center">
                    <input type="text" pattern=".{6,}" required title="minimo 6 caracteres"  name="rut_comercio" id="rut_comercio" required="required" placeholder="RUT del Comercio">
                </div>
                
                <div class="form-group text-center">
                    <input type="text" name="nombre_fantasia" id="nombre_fantasia" required="required" placeholder="Nombre de fantasía (Ejm: Botilleria Yapo)">
                </div>

                <div class="form-group text-center">
                    <input type="text" name="razon_social" id="razon_social" required="required" placeholder="Razón Social">
                </div>

                <div class="form-group text-center">
                    <input type="text" name="direccion" id="direccion" required="required" placeholder="Direccion de su comercio">
                </div>
                
                <div class="form-group text-center">
                    <input type="text" name="comuna" id="comuna" required="required" placeholder="Comuna">
                </div>
                
                    <div class="form-group text-center">
                    <input type="text" name="telefono" id="telefono" class="input1" required="required" placeholder="Teléfono">
                </div>
                
                <div class="form-group text-center">
                    <input type="text" name="email" id="email" required="required" placeholder="Email">
                </div>
                
                    <div class="form-group text-center">
                    <input type="password" pattern=".{8,12}" required title="8 a 12 caracteres" name="pass" id="pass" required="required" placeholder="Contraseña">
                    </div> 
                    
                <div class="form-group text-center">
                    <input type="password" pattern=".{8,12}" required title="8 a 12 caracteres" name="passconfirm" id="passconfirm" required="required" placeholder="Repita la contraseña">
                </div>
                
                <div class="checkbox text-center">
                    <input type="checkbox" class="checkbox" name="cb-autos" value=""> Acepto los <a href="#">términos y condiciones</a><br>
                </div>
            
                <div class="form-group text-center boton2">
                    <button type="submit" value="registrar" name="submit">
                        Siguiente
                    </button>
                </div>
            
            </form>

            </div>
        </div>
    </div>
</main>
@endsection