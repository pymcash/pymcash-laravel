<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Billetera extends Model
{
    protected $table = 'billetera'; // apuntar a la tabla de la base de datos
    public $timestamps = false; // Deshabilitar timestamps
    protected $primaryKey = 'rut';
    protected $fillable = [
        'rut','nombre','monto',
    ];

}
