<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rol extends Model
{
    protected $table = 'rol'; // apuntar a la tabla de la base de datos
    public $timestamps = false; // Deshabilitar timestamps

    public function users()
    {
        return $this->hasMany(User::class,'id');
    }
    public static function findByName($name)
    {
        return static::where(compact('name'))->first();
    }
}
