<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pymcash extends Model
{
    protected $table = 'pymcash'; // apuntar a la tabla de la base de datos
    public $timestamps = false; // Deshabilitar timestamps
    protected $fillable = [
        'monto', 'valor',
    ];
}
