<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EliminarChat extends Model
{
    protected $table = 'chats_deleted'; // apuntar a la tabla de la base de datos
    public $timestamps = false; // Deshabilitar timestamps
    protected $fillable = [
        'rut','chat_id'
    ];
}
