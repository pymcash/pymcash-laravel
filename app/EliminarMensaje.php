<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EliminarMensaje extends Model
{
    protected $table = 'messages_deleted'; // apuntar a la tabla de la base de datos
    public $timestamps = false; // Deshabilitar timestamps
    protected $fillable = [
        'rut', 'message_id','chat_id'
    ];

}
