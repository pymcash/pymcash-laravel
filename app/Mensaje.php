<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Mensaje extends Model
{
    protected $table = 'messages'; // apuntar a la tabla de la base de datos
    public $timestamps = false; // Deshabilitar timestamps
    protected $fillable = [
        'chat_id', 'rut','message','status','time'
    ];


}
