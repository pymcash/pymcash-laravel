<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Notificacion_Miembro_Badge extends Model
{
    protected $table = 'notificacion_miembro_badge'; // apuntar a la tabla de la base de datos
    public $timestamps = false; // Deshabilitar timestamps
    protected $fillable = [
        'notificacion_id','monto','rut_comercio'
    ];
}
