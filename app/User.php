<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Rol;
use App\Billetera;
use App\Comercio;
use App\Miembro;
class User extends Authenticatable
{
    use Notifiable;
    public $timestamps = false; // Deshabilitar timestamps

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'password','direccion','comuna','telefono','email',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    
    public function getName()
    {
        return $this->name;
    }
    public static function findByName($name)
    {
        return static::where(compact('name'))->first();
    }

    public function getRol()
    {
        return $this->belongsTo(Rol::class,'rol_id','id');
    }
    function getModel()
    {
      
        if ($this->hasRole("Miembro"))        
            return $this->belongsTo(Miembro::class,'rut','rut')->first();            
        else if ($this->hasRole("Comercio"))
            return $this->belongsTo(Comercio::class,'rut','rut')->first();
        

    }
    public function authorizeRoles($roles)
    {
        if ($this->hasAnyRole($roles)) 
        {
            return true;
        }
        abort(401, 'Esta acción no está autorizada.');
    }
    public function hasAnyRole($roles)
    {
        if (is_array($roles)) {
            foreach ($roles as $role) 
            {
                if ($this->hasRole($role)) 
                {
                    return true;
                }
            }
        }else 
        {
            if ($this->hasRole($roles)) 
            {
                return true;
            }
        }
        return false;
    }
    public function hasRole($role)
    {

        if (strcmp(strtolower($this->getRol->name),strtolower($role))==0) 
        {
            return true;
        }
        return false;
    }
    public function isAdmin()
    {

        if (strcmp(strtolower($this->getRol->name),strtolower('Administrador'))==0) 
        {
            return true;
        }
        return false;        

    }
    function getBilletera()
    {   
        if ($this->hasRole("Miembro"))
            return $this->belongsTo(Billetera::class,'rut','rut')->first()->monto;
        else if ($this->hasRole("Comercio"))
            return $this->belongsTo(Billetera::class,'rut','rut')->first()->pymcash;

    }
    function setBilletera($monto)
    {   


        if ($this->hasRole("Miembro"))        
        {
            $billetera = $this->belongsTo(Billetera::class,'rut','rut')->first();
            $billetera->monto = $monto;
            $billetera->save();
        }            
        else if ($this->hasRole("Comercio"))
        {
            $billetera = $this->belongsTo(Billetera::class,'rut','rut')->first();
            $billetera->pymcash = $monto;
            $billetera->save();
        }
        return $this;
    }
    public function Model_Create($sArray)
    {
        $dato = [ 'rut' => $this->rut ];
        $array = array_merge($dato,$sArray);
        if ($this->hasRole("Miembro"))        
            Miembro::create($array);
        else if ($this->hasRole("Comercio"))
            Comercio::create($array);

    }

    



}
