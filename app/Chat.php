<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Mensaje;

class Chat extends Model
{
    protected $table = 'chat'; // apuntar a la tabla de la base de datos
    public $timestamps = false; // Deshabilitar timestamps
    protected $fillable = [
        'id','rut_1', 'rut_2','last_time'
    ];

    function messages()
    {
        return $this->hasMany(Mensaje::class,'chat_id','id')->get();
    }
    function getUnread($mi_rut)
    {
        return count(Mensaje::whereRaw("chat_id = {$this->id} AND rut <> '{$mi_rut}' AND status = 'unread' ")->get());
    }
    
}
