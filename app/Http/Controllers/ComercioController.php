<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\MessageBag;
use Illuminate\Support\Facades\Validator;
use Auth;
use App\User;
use App\MovimientosComercio;
use App\MovimientosMiembro;
use App\Pymcash;
use App\Transaccion;
use App\Comercio;
use App\Miembro;
use App\Chat;
use App\Mensaje;
use App\Notificacion;
use App\Bloqueado;
use App\EliminarMensaje;
use App\EliminarChat;
use App\Http\Controllers\HomeController;
use Freshwork\Transbank\CertificationBagFactory;
use Freshwork\Transbank\TransbankServiceFactory;
use Freshwork\Transbank\RedirectorHelper;
use Freshwork\Transbank\CertificationBag;
use Illuminate\Contracts\Auth\Guard;



class ComercioController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth');
    }
    function getBadge()
    {
        $contador = 0;
        $user = Auth::user();
        $chats = Chat::whereRaw("rut_1 = '{$user->rut}' OR rut_2 = '{$user->rut}'")->get();
        foreach ($chats as $chat)        
            $contador+=$chat->getUnread($user->rut);
        return $contador;
    }
    function home()
    {   
        $user = Auth::user();
        $contador = $this->getBadge();
        return view("comercio.home_comercio",compact('user','contador'));
    }
    function billetera()
    {
        $user = Auth::user();
        $contador = $this->getBadge();
        return view('comercio.billetera_comercio',compact('user','contador'));
    }
    function datos()
    {
        $user = Auth::user();
        return view('comercio.datos_comercio',compact('user'));
    }
    function vender()
    {
        $contador = $this->getBadge();
        return view('comercio.vender',compact('contador'));
    }
    function comprar_pymcash()
    {
        $contador = $this->getBadge();
        return view('comercio.comprar_pymcash',compact('contador'));
    }
    function promocion()
    {
        $contador = $this->getBadge();
        return view('comercio.promocion',compact('contador'));
    }
    function promocion_mail()
    {   
        $contador = $this->getBadge();
        return view('comercio.promocion_mail',compact('contador'));
    }
    function promocion_chat()
    {   
        $contador = $this->getBadge();
        $miembros = Miembro::all();
        return view('comercio.promocion_chat',compact('contador','miembros'));
    }
    function insertar_mensaje($rut_destinatario)
    {
        $request = request()->all();
        $msg = $request['msg'];
        $mi_rut = Auth::user()->rut;
        $s = "rut_1 = '{$rut_destinatario}' AND rut_2 = '{$mi_rut}'  OR rut_2 = '{$rut_destinatario}' AND rut_1 = '{$mi_rut}'";
        $chat = Chat::whereRaw("rut_1 = '{$rut_destinatario}' AND rut_2 = '{$mi_rut}'  OR rut_2 = '{$rut_destinatario}' AND rut_1 = '{$mi_rut}'")->first();
        if ($chat==NULL)
            $chat = Chat::create([
                'rut_1' => $mi_rut,
                'rut_2' => $rut_destinatario,
            ]);
        
        Mensaje::create([
            'chat_id' => $chat->id,
            'rut' => $mi_rut,
            'message' => $msg,
            'status' => 'unread',
        ]);
    }
    function promocion_chat_enviar()
    {
        $checklist = request()->all()['checklist'];
        if (isset($checklist))
        {
            foreach ($checklist as $rut)            
                $this->insertar_mensaje($rut);
            
        }
        return redirect()->route('home');
    }
    function movimientos()
    {
        $user = Auth::user();
        $movimientos = MovimientosComercio::where("rut_comercio",$user->rut)->get();        
        return view('comercio.movimientos_comercio',compact('user','movimientos'));
    }
    function markers()
    {
        // MARKERS LOCALIZAR USUARIOS / MIEMBROS
        $usuarios = User::where("rol_id","2")->get();
        return response()
        ->view('comercio.markers_usuario',compact("usuarios"))
        ->header('Content-Type', 'text/xml');

    }
    function localizar()
    {  
        $contador = $this->getBadge();
        return view('comercio.localizar_usuario',compact('contador'));
    }
    
    function vender_action()
    {
        $user = Auth::user();
        $request = request()->all(); 
        $miembro = User::find($request['miembro']);
        $request['rut'] = $miembro ? $miembro->rut : '0';
        
        // VALIDANDO MIEMBRO INGRESADO
        $validator = Validator::make($request, [
            'miembro'               => 'exists:users,id',
            'rut'                   => 'exists:miembros,rut'
            
        ]);                        
        if ($validator->fails())                             
            return back()->withErrors($validator)->withInput();
        
        
 
        // OBTENIENDO VALOR DEL PYMCASH
        $pym = Pymcash::whereRaw("target_rol = 2 AND cant_moneda = 1")->first();
        if (!isset($pym))
            dd("NOT FOUND PYMCASH VALUE");            


        // CALCULANDO PYM_CASH
        for ($pym_moneda = 0,$valor=$request['monto']; $valor>=$pym->target ;$pym_moneda++)
        {   
            $valor-= $pym->target;
            $valor = floor($valor);
        }   
        

        // VALIDANDO SALDO PYMCASH
        $request['pymcash'] = $user->getBilletera()-$pym_moneda;
        $validator = Validator::make($request, [
            'pymcash'               => 'required|regex:/^\s*(?=.*[0-9])\d*(?:\.\d{1,2})?\s*$/'
            
        ]);                        
        if ($validator->fails())                     
            return back()->withErrors($validator)->withInput();
        

        // REALIZANDO OPERACION DE DESCONTAR PYMCASH
        
        $user->setBilletera($user->getBilletera()-$pym_moneda);
        $user->save();

        // AÑADIR PYMCASH A MIEMBRO

        $miembro->setBilletera($miembro->getBilletera()+($pym_moneda*$pym->valor));
        $miembro->save();


        // REGISTRANDO VENTA

        MovimientosComercio::create([
            'rut_comercio' => $user->rut,
            'rut_miembro'  => $miembro->rut,
            'detalle'      => 'Venta Cliente',
            'monto'        => $request['monto'],
            'pymcash'      => $pym_moneda,
            'boleta'       => $request['boleta'],       
        ]);

        // GENERANDO MOVIMIENTO AL MIEMBRO

        MovimientosMiembro::create([
            'rut'        => $miembro->rut,
            'detalle'    => 'Bonificacion por compra',
            'monto'      => $pym->valor * $pym_moneda,        
        ]);

        // GENERAR NOTIFICACION

        Notificacion::create([
            'titulo' => 'Bonifiación por compra',
            'mensaje' => 'has recibido '.$pym_moneda.' PymCash',
            'rut_target' => $miembro->rut,
            'status' => 'unread',
        ])->crear_badge_miembro([
            "monto" => $pym->valor * $pym_moneda,
            "rut_comercio" => $user->rut
        ]);

        $contador = $this->getBadge();
        return view("comercio.venta_exitosa",compact('contador'));
        
    }

    function comprar_pymcash_previa()
    {
        $cantidad = request()->all()['cantidad'];
        $pym = Pymcash::whereRaw("target_rol = 3 AND cant_moneda = 10")->first();
        $monto = $pym->valor * ($pym->cant_moneda * $cantidad);
        $total = $monto+($monto*0.19);
        $contador = $this->getBadge();
        return view('comercio.comprar_pymcash_preview',compact('cantidad','monto','total','contador'));
    }

    function mostrar_mensajeria()
    {

        $ruts = Bloqueado::get(['rut'])->groupBy('rut')->keys()->all();
        $miembros = Miembro::whereNotIn('rut',$ruts)
                            ->orderBy('nombre', 'ASC')
                            ->get();   
        $contador = $this->getBadge();
        return view('comercio.mensajeria',compact('miembros','contador'));
    }

    function mostrar_chat($rut)
    {
        $miembro = Miembro::where("rut",$rut)->first();
        if (!$miembro)
            return redirect()->route('mensajeria');


        $mi_rut = Auth::user()->rut;
        $nombre = Auth::user()->getModel()->nombre_fantasia;
        $chat = Chat::whereRaw("rut_1 = '{$rut}' AND rut_2 = '{$mi_rut}'  OR rut_2 = '{$rut}' AND rut_1 = '{$mi_rut}'")->first();
        $mensajes = NULL;
        if ($chat!==NULL)
        {
            $mensajes = Mensaje::where("chat_id","{$chat->id}")->get();
            // LEER MENSAJES
            Mensaje::whereRaw("chat_id = {$chat->id} AND rut = '{$rut}' ")->update(["status" => "read" ]);

            //FILTRANDO LOS MENSAJES ELIMINADOS
            $msgs = EliminarMensaje::whereRaw("rut = '$mi_rut'")->get(['message_id'])->groupBy('message_id')->keys()->all();                        
            $mensajes = $mensajes->whereNotIn('id',$msgs);

        }       
            
        

        $user = Auth::user();
        $model = User::class;
        $miembro_rut = $rut;

        
        $contador = $this->getBadge();
        return view('comercio.chat',compact('miembro','nombre','mensajes','user','model','miembro_rut','contador'));
    }
    function mostrar_chat_list()
    {
        
        $ruts = Bloqueado::get(['rut'])->groupBy('rut')->keys()->all();        
        $mi_rut = Auth::user()->rut;
        $chats = Chat::whereRaw("rut_1 = '$mi_rut' OR rut_2 = '{$mi_rut}'")                        
                       ->orderBy('last_time', 'DESC')->get();

        // FILTRANDO LOS CHATS ELIMINADOS
        $chats_eliminados = EliminarChat::whereRaw("rut = '$mi_rut'")->get(['chat_id'])->groupBy('chat_id')->keys()->all();                        
        $chats = $chats->whereNotIn('id',$chats_eliminados);

        //FILTRANDO LOS CHATS DE COMERCIOS BLOQUEADOS
        $chats = $chats->whereNotIn('rut_1',$ruts);
        $chats = $chats->whereNotIn('rut_2',$ruts);   

        $user = User::class;
        $auth = Auth::user();
        $Mensaje = Mensaje::class;
        $EliminarMensaje = EliminarMensaje::class;
        $contador = $this->getBadge();
        return view('comercio.chat_lista',compact('chats','user','auth','Mensaje','contador','EliminarMensaje'));
    }
    function mostrar_bloqueados()
    {
        $contador = $this->getBadge();
        $rut = Auth::user()->rut;
        $bloqueados = Bloqueado::whereRaw("blocked_by = '$rut'")->get();
        return view('comercio.bloqueado',compact('contador','bloqueados'));
    }


}
