<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
class principal extends Controller
{
    //
    function index()
    {
        if (Auth::user()!==null)
            return redirect('home');
         else     
            return view('index');
    }
    function crear_usuario()
    {
        return view('crear_usuario');
    }
    function crear_comercio()
    {
        return view('crear_comercio');
    }
    function iniciar_sesion()
    {
        return view('login');
    }
    function recuperar_password()
    {
        return view('recuperar_password');
    }
    function choose_create()
    {
        return view('choose_create');
    }
}
