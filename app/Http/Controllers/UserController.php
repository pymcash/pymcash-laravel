<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\MessageBag;
use Illuminate\Support\Facades\Validator;
use Auth;
use App\Http\Controllers\UserController;
use App\User;
use App\Comercio;
use App\Chat;
use App\Mensaje;
use App\MovimientosComercio;
use App\MovimientosMiembro;
use App\Pymcash;
use App\Transaccion;
use App\Notificacion;
use App\Bloqueado;
use App\EliminarMensaje;
use App\EliminarChat;
use Freshwork\Transbank\CertificationBagFactory;
use Freshwork\Transbank\TransbankServiceFactory;
use Freshwork\Transbank\RedirectorHelper;
use Freshwork\Transbank\CertificationBag;
use Illuminate\Contracts\Auth\Guard;



class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    function getBadge()
    {
        $contador = 0;
        $user = Auth::user();
        $chats = Chat::whereRaw("rut_1 = '{$user->rut}' OR rut_2 = '{$user->rut}'")->get();
        foreach ($chats as $chat)        
            $contador+=$chat->getUnread($user->rut);

        $notificaciones = Notificacion::whereRaw("rut_target = '$user->rut' AND status = 'unread' ")->get();
        return $contador+count($notificaciones);
    }
    function getNotifyBadge()
    {
        $user = Auth::user();
        $notificaciones = Notificacion::whereRaw("rut_target = '$user->rut' AND status = 'unread' ")->get();
        $contador = count($notificaciones);
        return $contador;
    }
    public function home()
    {   
        $user = Auth::user();
        $contador = $this->getBadge();        
        return view('user.home_user',compact('user','contador'));
    }

    function billetera()
    {   
        $user = Auth::user();
        $contador = $this->getBadge();    
        return view('user.billetera_user',compact('user','contador'));
    }
    function datos()
    {
        $user = Auth::user();
        
        return view('user.datos_usuario',compact('user'));
    }

    function movimientos()
    {
        $user = Auth::user();
        $movimientos = MovimientosMiembro::where("rut",$user->rut)->get(); 
        return view('user.movimientos_user',compact('user','movimientos'));
    }

    function markers()
    {
        
         $comercios = User::where("rol_id","3")->get();
        return response()
        ->view('user.markers_comercio',compact("comercios"))
        ->header('Content-Type', 'text/xml');

    }
    function localizar()
    {
        $contador = $this->getBadge();    
        return view('user.localizar_comercio',compact('contador'));
    }

    function invitar_amigo()
    {
        $contador = $this->getBadge();    
        return view('user.invitar_amigo',compact('contador'));
    }

    function mostrar_chat($rut)
    {
        $comercio = Comercio::where("rut",$rut)->first();
        if (!$comercio)
            return redirect()->route('mensajeria');


        $comercio_rut = $rut;
        $mi_rut = Auth::user()->rut;
        $nombre = Auth::user()->getModel()->nombre." ".Auth::user()->getModel()->apellido;
        $chat = Chat::whereRaw("rut_1 = '{$rut}' AND rut_2 = '{$mi_rut}'  OR rut_2 = '{$rut}' AND rut_1 = '{$mi_rut}'")->first();
        $mensajes = NULL;
        if ($chat!==NULL)    
        {
            $mensajes = Mensaje::where("chat_id","{$chat->id}")->get();
            // LEER MENSAJES
            Mensaje::whereRaw("chat_id = {$chat->id} AND rut = '{$comercio_rut}' ")->update(["status" => "read" ]);

            //FILTRANDO LOS MENSAJES ELIMINADOS
            $msgs = EliminarMensaje::whereRaw("rut = '$mi_rut'")->get(['message_id'])->groupBy('message_id')->keys()->all();                        
            $mensajes = $mensajes->whereNotIn('id',$msgs);
        }    
            
        $user = Auth::user();
        $model = User::class;
        
        $contador = $this->getBadge();    
        return view('user.chat',compact('comercio','nombre','mensajes','user','model','comercio_rut','contador','chat'));
    }

    function mostrar_mensajeria()
    {

            
       $ruts = Bloqueado::get(['rut'])->groupBy('rut')->keys()->all();
       $comercios = Comercio::whereNotIn('rut',$ruts)
                            ->orderBy('nombre_fantasia', 'ASC')                            
                            ->get();
        $contador = $this->getBadge();
        $notify_count = $this->getNotifyBadge();  
        return view('user.mensajeria',compact('comercios','contador','notify_count'));
    }
    function mostrar_bloqueados()
    {
        $contador = $this->getBadge();
        $notify_count = $this->getNotifyBadge();  
        $rut = Auth::user()->rut;
        $bloqueados = Bloqueado::whereRaw("blocked_by = '$rut'")->get();
        return view('user.bloqueado',compact('contador','notify_count','bloqueados'));
    }

    function mostrar_chat_list()
    {
        $ruts = Bloqueado::get(['rut'])->groupBy('rut')->keys()->all();        
        $mi_rut = Auth::user()->rut;
        
        $chats = Chat::whereRaw("rut_1 = '$mi_rut' OR rut_2 = '{$mi_rut}'")                        
                                   ->orderBy('last_time', 'DESC')->get();

        // FILTRANDO LOS CHATS ELIMINADOS
        $chats_eliminados = EliminarChat::whereRaw("rut = '$mi_rut'")->get(['chat_id'])->groupBy('chat_id')->keys()->all();                        
        $chats = $chats->whereNotIn('id',$chats_eliminados);

        //FILTRANDO LOS CHATS DE COMERCIOS BLOQUEADOS
        $chats = $chats->whereNotIn('rut_1',$ruts);
        $chats = $chats->whereNotIn('rut_2',$ruts);        
        $user = User::class;
        $auth = Auth::user();
        $Mensaje = Mensaje::class;
        $EliminarMensaje = EliminarMensaje::class;
        $contador = $this->getBadge();  
        $notify_count = $this->getNotifyBadge();  
        return view('user.chat_lista',compact('chats','user','auth','Mensaje','contador','notify_count','EliminarMensaje'));
    }
    function notificaciones()
    {
        $contador = $this->getBadge();    
        $rut = Auth::user()->rut;
        $notificaciones = Notificacion::whereRaw("rut_target = '$rut'")->get();
        $notify_count = $this->getNotifyBadge();  
        return view('user.notificaciones',compact('contador','notificaciones','notify_count'));
    }
    function notificacion_pymcash_compra_comercio($id)
    {
        $contador = $this->getBadge();    
        $rut = Auth::user()->rut;
        $notificacion = Notificacion::find($id)->first();
        $notificacion->status = 'read';
        $notificacion->save();
        $rut_comercio = $notificacion->get_miembro_badge()->first()->rut_comercio;
        $monto = $notificacion->get_miembro_badge()->first()->monto;
        $comercio = User::whereRaw("rut = '{$rut_comercio}'")->first()->getModel();        
        return view('user.compra_comercio',compact('contador','comercio','monto'));
    }
}
