<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\MessageBag;
use Illuminate\Support\Facades\Validator;
use Auth;
use App\Http\Controllers\UserController;
use App\User;
use App\MovimientosComercio;
use App\MovimientosMiembro;
use App\Pymcash;
use App\Transaccion;
use Freshwork\Transbank\CertificationBagFactory;
use Freshwork\Transbank\TransbankServiceFactory;
use Freshwork\Transbank\RedirectorHelper;
use Freshwork\Transbank\CertificationBag;
use Illuminate\Contracts\Auth\Guard;

class TransbankController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    function transbank_compra_fallida(Request $request, Guard $auth)
    {        
      $transaccion = Transaccion::whereRaw("rut = '".Auth::user()->rut."' AND status = 'iniciando' AND token = '".$request->session()->get('token_ws')."'")->first();    
      $transaccion->status = 'fallido';
      $transaccion->save();
      return view("comercio.compra_pymcash_error");
    }
    
    function transbank_compra_finalizada(Request $request, Guard $auth)
    {
       
      $transaccion = Transaccion::whereRaw("rut = '".Auth::user()->rut."' AND status = 'procesando' AND token = '".$request->request->getAlnum('token_ws')."'")->first();
      $transaccion->status = 'finalizado';
      $transaccion->save();
      $comercio = Auth::user();

      $comercio->setBilletera($comercio->getBilletera()+($transaccion->pym_pack_qty*10));
      $comercio->save();

      MovimientosComercio::create([
        'rut_comercio' => $comercio->rut,
        'rut_miembro'  => '',
        'detalle'      => 'Compra pym cash',
        'monto'        => $transaccion->monto,
        'pymcash'      => $transaccion->pym_pack_qty*10,
        'boleta'       => $transaccion->buyOrder,       
    ]);
        $pymcash = $transaccion->pym_pack_qty*10;
        return view("comercio.compra_pymcash_exitosa",compact('pymcash'));

    }

    function transbank_procesar_compra(Guard $auth)
    {
        $bag = CertificationBagFactory::integrationWebpayNormal();

        $plus = TransbankServiceFactory::normal($bag);

        $response = $plus->getTransactionResult();

        if ($response->detailOutput->responseCode == -1 ) // RECHAZADO         
            return redirect()->route('compra_fallida')->with('token_ws',request()->all()['token_ws']);


        $transaccion = Transaccion::whereRaw("rut = '".Auth::user()->rut."' AND token = '".request()->all()['token_ws']."'")->first();
        $transaccion->status = 'procesando';
        $transaccion->save();

        //Si todo está bien, peudes llamar a acknowledgeTransaction. Si no se llama a este método, la transaccion se reversará en 30 segundos.
        $plus->acknowledgeTransaction();
        
        //Redirect back to Webpay Flow and then to the thanks page
        return RedirectorHelper::redirectBackNormal($response->urlRedirection);
    }

    function transbank_preparar_compra_pymcash_pack()
    {

        //Get a certificationBag with certificates and private key of WebpayNormal for integration environment.
        $bag = CertificationBagFactory::integrationWebpayNormal();
        $plus = TransbankServiceFactory::normal($bag);

        $request = request()->all();
        $Qty = $request['cantidad'];
        $pym = Pymcash::whereRaw("target_rol = 3 AND cant_moneda = 10")->first();
        $monto = $pym->valor * ($pym->cant_moneda * $Qty) ;
        $monto = $monto + ($monto*0.19);
        $buyOrder = rand (100000,65000);
        //Para transacciones normales, solo se puede añadir una linea de detalle de transacción.
        $plus->addTransactionDetail($monto, $buyOrder); //Amount and BuyOrder

        $response = $plus->initTransaction(route('procesando'), route('compra_finalizada'));

        Transaccion::create([
            'rut' => Auth::user()->rut,
            'pym_pack_qty' => $Qty,
            'monto' => $monto,
            'buyOrder' => $buyOrder,
            'status' => 'iniciando',
            'token' => $response->token

        ]);
        echo RedirectorHelper::redirectHTML($response->url, $response->token);

    }

}
