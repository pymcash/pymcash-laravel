<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \Mailjet\Resources;
use \Mailjet\Client;

class MailjetController extends Controller
{
   private $apikey,$apisecret,$client,$recipients,$subject;
   function __construct()
   {
    
    $this->apikey    = 'e50a04a645985634bfdce3a45a78f5bc';        
    $this->apisecret = 'dd4d302ca5e1b1f7ce629197a7da801b';
    $this->client    = new \Mailjet\Client($this->apikey, $this->apisecret);
    $this->recipients = array();
    
   }
   function addRecipient($recipient)
   {
        $this->recipients[count($this->recipients)] = [ 'Email' => $recipient];
   }
   function subject($title)
   {
       $this->subject = $title;
   }
   function send()
   {
   
    $email = [
        'FromName'     => 'Pymcash',
        'FromEmail'    => 'noreply@sistema3esferas.com',
        'Text-Part'    => '<h3>Simple Email test</h3>',
        'Subject'      => $this->subject,
        'Html-Part'    => 'Simple Email Test',
        'Recipients'   => $this->recipients,
        //'MJ-custom-ID' => 'Hello ID',
      ];
      return $this->client->post(Resources::$Email, ['body' => $email]);
   }
}
