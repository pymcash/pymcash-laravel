<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class admin extends Controller
{
    function principaladmin()
    {
        return view('admin/paneladmin');
    }
    function adminusers(){
    	return view('admin/adminusers');
    }
    function adminusers_edit(){
    	return view('admin/adminusers_edit');
    }
    function admincommerce(){
    	return view('admin/admincommerce');
    }
    function admincommerce_edit(){
    	return view('admin/admincommerce_edit');
    }
}
