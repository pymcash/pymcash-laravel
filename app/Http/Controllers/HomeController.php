<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\MessageBag;
use Illuminate\Support\Facades\Validator;
use Auth;
use App\Http\Controllers\UserController;
use App\Http\Controllers\ComercioController;
use App\User;
use App\MovimientosComercio;
use App\MovimientosMiembro;
use App\Pymcash;
use App\Transaccion;
use App\Chat;
use App\Mensaje;
use App\Notificacion;
use App\Bloqueado;
use App\EliminarMensaje;
use App\EliminarChat;
use Freshwork\Transbank\CertificationBagFactory;
use Freshwork\Transbank\TransbankServiceFactory;
use Freshwork\Transbank\RedirectorHelper;
use Freshwork\Transbank\CertificationBag;
use Illuminate\Contracts\Auth\Guard;

use App\Http\Controllers\MailjetController;

include '../vendor/autoload.php';


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    private $UserController,$ComercioController;
    protected $reserved;
    public function __construct()
    {
        $this->middleware('auth');
        $this->UserController = new UserController();
        $this->ComercioController = new ComercioController();
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    function mostrar_cambiar_contraseña()
    {
        $user = Auth::user();
        if ($user->hasRole("Miembro"))            
            return view('user.cambiar_contraseña');    
        else if ($user->hasRole("Comercio"))
            return view('comercio.cambiar_contraseña');            
        
    }
    function cambiar_contraseña()
    {
        $user = Auth::user();
        $request = request()->all(); 
        
        $validator = Validator::make($request, [
            'password'               => 'required|confirmed',
        ]);            
        if ($validator->fails())     
        {
            
            return back()->withErrors($validator)->withInput();
        }            
            
        
        $datos = [
            "password" => bcrypt($request['password']),
            
        ];
        $user->fill($datos); 
        $user->save();
        return $this->mostrar_home();

    }

    function mostrar_billetera()
    {
        $user = Auth::user();
        if ($user->hasRole("Miembro"))            
            return $this->UserController->billetera();
        else if ($user->hasRole("Comercio"))
            return $this->ComercioController->billetera();
    }
    function mostrar_home()
    {
        $user = Auth::user();
        if ($user->hasRole("Miembro"))            
            return $this->UserController->home();         
        else if ($user->hasRole("Comercio"))
            return $this->ComercioController->home();
    }
    function mostrar_datos()
    {
        $user = Auth::user();
        if ($user->hasRole("Miembro"))            
            return $this->UserController->datos();              
        else if ($user->hasRole("Comercio"))
            return $this->ComercioController->datos();
    }
    function localizar()
    {
        $user = Auth::user();
        if ($user->hasRole("Miembro"))            
            return $this->UserController->localizar();                
        else if ($user->hasRole("Comercio"))
            return $this->ComercioController->localizar();
    }
    function mostrar_movimientos()
    {
        $user = Auth::user();
        if ($user->hasRole("Miembro"))            
            return $this->UserController->movimientos();              
        else if ($user->hasRole("Comercio"))
            return $this->ComercioController->movimientos();
        
            
    }
    function generar_markers()
    {
        $user = Auth::user();
        if ($user->hasRole("Miembro"))            
            return $this->UserController->markers();             
        else if ($user->hasRole("Comercio"))
            return $this->ComercioController->markers();
    }
    function editar_datos()
    {
        $user = Auth::user();
        $request = request()->all(); 
        
        $validator = Validator::make($request, [
            'email'               => 'required|email|unique:users,email,'.$user->id,
        ]);            
        if ($validator->fails())                 
            return back()->withErrors($validator)->withInput();
        
        $datos = [
            "direccion" => $request['direccion'],
            "comuna"    => $request['comuna'],
            "telefono"  => $request['telefono'],
            "email"     => $request['email'],
        ];
        $user->fill($datos); 
        $user->save();
           
        return $this->mostrar_datos();
    }
    function mostrar_chat($rut)
    {
        
        $user = Auth::user();
        if ($user->hasRole("Miembro"))            
            return $this->UserController->mostrar_chat($rut);        
        else if ($user->hasRole("Comercio"))
            return $this->ComercioController->mostrar_chat($rut);
    }
    function mostrar_mensajeria()
    {
        $user = Auth::user();
        if ($user->hasRole("Miembro"))            
            return $this->UserController->mostrar_mensajeria();        
        else if ($user->hasRole("Comercio"))
            return $this->ComercioController->mostrar_mensajeria();
    }
    function chat_lista()
    {

        $user = Auth::user();
        if ($user->hasRole("Miembro"))            
            return $this->UserController->mostrar_chat_list();       
        else if ($user->hasRole("Comercio"))
            return $this->ComercioController->mostrar_chat_list();
    }

    function insertar_mensaje($rut_destinatario)
    {
        $request = request()->all();
        $msg = $request['msg'];
        $mi_rut = Auth::user()->rut;
       // $s = "rut_1 = '{$rut_destinatario}' AND rut_2 = '{$mi_rut}'  OR rut_2 = '{$rut_destinatario}' AND rut_1 = '{$mi_rut}'";
        $chat = Chat::whereRaw("rut_1 = '{$rut_destinatario}' AND rut_2 = '{$mi_rut}'  OR rut_2 = '{$rut_destinatario}' AND rut_1 = '{$mi_rut}'")->first();
        if ($chat==NULL)
            $chat = Chat::create([
                'rut_1' => $mi_rut,
                'rut_2' => $rut_destinatario,
            ]);

        // SI UNO DE LOS USUARIOS ELIMINO EL CHAT, AL ENVIAR UN MENSAJE EL CHAT SE "REABRE"
            $result = EliminarChat::whereRaw("chat_id = $chat->id AND rut = '{$mi_rut}' OR chat_id = $chat->id AND rut <> '{$mi_rut}'")->get();
            foreach ($result as $chat_eliminado)            
                $chat_eliminado->delete();

        Mensaje::create([
            'chat_id' => $chat->id,
            'rut' => $mi_rut,
            'message' => $msg,
            'status' => 'unread',
        ]);
        
    }
    function mostrar_notificaciones()
    {
        $user = Auth::user();
        if ($user->hasRole("Miembro"))            
            return $this->UserController->notificaciones();       
        else if ($user->hasRole("Comercio"))
            return $this->ComercioController->notificaciones();
    }
    function eliminar_notificacion($id)
    {
       $user = Auth::user();
       $notificacion = Notificacion::whereRaw("id = $id AND rut_target = '$user->rut' ")->first(); 
       if ($notificacion!==NULL)
        $notificacion->delete();
       return redirect()->route('notificaciones');
    }
    function eliminar_notificaciones()
    {
        $user = Auth::user();        
        $collection = Notificacion::whereRaw("rut_target = '$user->rut'")->get();
        if (count($collection)>0)
            Notificacion::destroy($collection->toArray());

        return redirect()->route('notificaciones');
    }
    function bloquear($rut)
    {
       $element = User::whereRaw("rut = '$rut'")->get();
       if (count($element)>0)
       {

            $mi_rut = Auth::user()->rut;
            $element = Bloqueado::whereRaw("rut='$rut' AND blocked_by = '$mi_rut' ")->get();
            if (count($element)==0)
            Bloqueado::create([
                'rut' => $rut,
                'blocked_by' => Auth::user()->rut
            ]);

       }
      return redirect()->route('chat_lista');
    }
    function desbloquear($rut)
    {

        $element = User::whereRaw("rut = '$rut'")->get();
        if (count($element)>0)
        {
            $mi_rut = Auth::user()->rut;
            Bloqueado::whereRaw("rut='$rut' AND blocked_by = '$mi_rut' ")->delete();
 
        }
        return redirect()->route('bloqueados');
    }

    function send_mail()
    {

        $Mailjet = new MailJetController();
        $Mailjet->addRecipient('oscar_farias111@hotmail.com');
        $Mailjet->addRecipient('microcybort@gmail.com');
        $Mailjet->subject('Probando controlador');
        dd($Mailjet->send());
        
    }
    function mostrar_bloqueados()
    {
        $user = Auth::user();
        if ($user->hasRole("Miembro"))            
            return $this->UserController->mostrar_bloqueados();       
        else if ($user->hasRole("Comercio"))
            return $this->ComercioController->mostrar_bloqueados();
    }
    function vaciar_chat($rut, $Redirect = true)
    {
        $mi_rut = Auth::user()->rut;
        $chat = Chat::whereRaw("rut_1 = '{$rut}' AND rut_2 = '{$mi_rut}'  OR rut_2 = '{$rut}' AND rut_1 = '{$mi_rut}'")->first();        
        $mensajes = NULL;
        if ($chat!==NULL)    
            $mensajes = Mensaje::where("chat_id","{$chat->id}")->get();
        
        
        if ($mensajes!==NULL)
        {

            // ESTRUCTURA DE DATOS RESERVADA
            $this->reserved = [
                'contador' => 0,
                'rut' => $mi_rut,
                'trash' => array(),
            ];

            // PROPIEDAD TRASH CONTENDRA LA INFORMACION EN UN ARREGLO 
            // QUE SERA OCULTADA PARA EL
            // USUARIO QUE ESTA VACIANDO EL CHAT

            $mensajes->each(function ($item, $key) {                
                $this->reserved['trash'][$this->reserved['contador']++] = [
                    "rut" => $this->reserved['rut'],
                    "message_id" => $item->id,
                    "chat_id" => $item->chat_id,
                ];
            });

            EliminarMensaje::insert($this->reserved['trash']);

            
        }

        if ($Redirect)
            return redirect()->route('chat',['rut' => $rut]);

       
    }
    function eliminar_chat($rut)
    {
        $this->vaciar_chat($rut,false);
        $mi_rut = Auth::user()->rut;
        $chat = Chat::whereRaw("rut_1 = '{$rut}' AND rut_2 = '{$mi_rut}'  OR rut_2 = '{$rut}' AND rut_1 = '{$mi_rut}'")->first();
        if ($chat)
        {
            // LEER MENSAJES
            Mensaje::whereRaw("chat_id = {$chat->id} AND rut <> '{$mi_rut}' ")->update(["status" => "read" ]);

            EliminarChat::create([
                "rut" => Auth::user()->rut,
                'chat_id' => $chat->id,
            ]);
        }
        return redirect()->route('chat_lista');
    }

}
