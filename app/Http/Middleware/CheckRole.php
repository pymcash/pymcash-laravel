<?php

namespace App\Http\Middleware;

use Closure;
use App\Rol;
class CheckRole
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, ...$roles)
    {
        if ($request->user()->hasRole("Desactivado"))
        return redirect()->route('desactivado');
        if ( $request->user()->authorizeRoles($roles)) {
            
            return $next($request);
        }
        return abort(401, 'Esta acción no está autorizada.');

    }
}
