<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bloqueado extends Model
{
    protected $table = 'bloqueados'; // apuntar a la tabla de la base de datos
    public $timestamps = false; // Deshabilitar timestamps
    protected $fillable = [
        'id','rut','blocked_by',
    ];

    function getRut()
    {
        return $this->rut;
    }
    public function getModel()
    {
        $model = $this->belongsTo(User::class,'rut','rut')->first();
        return $model->getModel();
    }
}
