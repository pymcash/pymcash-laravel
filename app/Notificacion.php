<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Notificacion_Miembro_Badge;
class Notificacion extends Model
{
    protected $table = 'notificaciones'; // apuntar a la tabla de la base de datos
    public $timestamps = false; // Deshabilitar timestamps
    protected $fillable = [
        'id','titulo','mensaje','rut_target','status','fecha',
    ];


    function crear_badge_miembro($array)
    {  
        $dato = ["notificacion_id" => $this->id];
       return Notificacion_Miembro_Badge::create(array_merge($dato,$array));
    }
    function get_miembro_badge()
    {
        return $this->belongsTo(Notificacion_Miembro_Badge::class,'id','notificacion_id')->get();
    }
}