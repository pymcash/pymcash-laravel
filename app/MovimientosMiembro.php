<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MovimientosMiembro extends Model
{
    protected $table = 'movimientos_miembros'; // apuntar a la tabla de la base de datos
    public $timestamps = false; // Deshabilitar timestamps
    protected $fillable = [
        'rut','monto','detalle','nro_cuenta','fecha',
    ];
}
