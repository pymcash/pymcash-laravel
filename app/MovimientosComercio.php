<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MovimientosComercio extends Model
{
    protected $table = 'movimientos_comercio'; // apuntar a la tabla de la base de datos
    public $timestamps = false; // Deshabilitar timestamps
    protected $fillable = [
        'rut_comercio','rut_miembro','monto','detalle','pymcash','boleta','fecha',
    ];
}
