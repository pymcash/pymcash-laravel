<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transaccion extends Model
{
    protected $table = 'transaccion'; // apuntar a la tabla de la base de datos
    public $timestamps = false; // Deshabilitar timestamps
    protected $fillable = [
        'id', 'rut','pym_pack_qty','monto','buyOrder','token','fecha','status',
    ];
}
