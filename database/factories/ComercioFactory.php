<?php

use Faker\Generator as Faker;

$factory->define(App\User::class, function (Faker $faker) {
    return [
        'rut' => rand(15000000,90000000).'-'.rand(1,9),
        'email' => $faker->unique()->safeEmail,
        'password' => bcrypt('1'),
        'rol_id' => '3',
        'direccion' => $faker->streetAddress,
        'comuna' => $faker->streetName,
        'telefono' => $faker->phoneNumber,
        'latitud' => $faker->latitude(-33.373538, -33.490485),
        'longitud' => $faker->longitude(-70.457886, -70.657886),
    ];
});
