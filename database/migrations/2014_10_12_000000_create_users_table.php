<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::defaultStringLength(191);

        Schema::create('rol',function(Blueprint $table){
            $table->increments('id');
            $table->string('name');
        });
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('rut')->unique();            
            $table->unsignedInteger('rol_id');
            $table->foreign('rol_id')->references('id')->on('rol');
            $table->string('email')->unique();
            $table->string('password');
            $table->string('direccion');
            $table->string('comuna');
            $table->string('telefono');
            $table->string('latitud')->nullable();
            $table->string('longitud')->nullable();
            $table->rememberToken();
           // $table->timestamps();
        });

        Schema::create('miembros',function(Blueprint $table){
            $table->string('rut')->unique();
            $table->foreign('rut')->references('rut')->on('users')->onDelete('cascade');
            $table->string('nombre');
            $table->string('apellido');
            $table->dateTime('fecha_nacimiento')->default(DB::raw('CURRENT_TIMESTAMP'));
        });
        Schema::create('comercios', function (Blueprint $table) {
            $table->string('rut')->unique();
            $table->foreign('rut')->references('rut')->on('users')->onDelete('cascade');          
            $table->string('nombre_fantasia');
            $table->string('razon_social');
    
        });

        Schema::create('billetera',function(Blueprint $table){
            $table->string('rut')->unique();
            $table->foreign('rut')->references('rut')->on('users')->onDelete('cascade');  
            $table->unsignedInteger('monto')->default(0);
            $table->unsignedInteger('pymcash')->default(0);
        });

        Schema::create('pymcash',function(Blueprint $table){
            $table->unsignedInteger('cant_moneda')->index();
            $table->unsignedInteger('valor');
            $table->unsignedInteger('target_rol');
            $table->foreign('target_rol')->references('id')->on('rol')->onDelete('cascade');  
            $table->unsignedInteger('target');
        });

        Schema::create('movimientos_comercio', function (Blueprint $table) {
            $table->increments('id');
            $table->string('rut_comercio');
            $table->foreign('rut_comercio')->references('rut')->on('comercios')->onDelete('cascade');   
            $table->string('rut_miembro')->nullable()->default('');
            $table->string('detalle');
            $table->unsignedInteger('monto');
            $table->unsignedInteger('pymcash');
            $table->unsignedInteger('boleta');
            $table->dateTime('fecha')->default(DB::raw('CURRENT_TIMESTAMP'));

        });

        Schema::create('movimientos_miembros', function (Blueprint $table) {
            $table->increments('id');
            $table->string('rut');
            $table->foreign('rut')->references('rut')->on('miembros')->onDelete('cascade');           
            $table->string('detalle');
            $table->unsignedInteger('monto');
            $table->string('nro_cuenta')->default('0000-0000-0000');
            $table->dateTime('fecha')->default(DB::raw('CURRENT_TIMESTAMP'));

        });


        Schema::create('transaccion',function(Blueprint $table){
            $table->increments('id');
            $table->string('rut');
            $table->foreign('rut')->references('rut')->on('comercios')->onDelete('cascade');
            $table->unsignedInteger('pym_pack_qty');
            $table->double('monto');
            $table->string('buyOrder',100);
            $table->dateTime('fecha')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->string('status');
            $table->string('token');
            

        });

        Schema::create('chat', function (Blueprint $table) {
            $table->increments('id');
            $table->string('rut_1');
            $table->foreign('rut_1')->references('rut')->on('users')->onDelete('cascade');
            $table->string('rut_2');
            $table->foreign('rut_2')->references('rut')->on('users')->onDelete('cascade');
            $table->dateTime('last_time')->default(DB::raw('CURRENT_TIMESTAMP'));

        });

        Schema::create('messages', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('chat_id');
            $table->foreign('chat_id')->references('id')->on('chat')->onDelete('cascade');
            $table->string('rut');
            $table->foreign('rut')->references('rut')->on('users')->onDelete('cascade');            
            $table->string('message');
            $table->string('status');
            $table->dateTime('time')->default(DB::raw('CURRENT_TIMESTAMP'));
        });

        Schema::create('notificaciones', function (Blueprint $table) {
            $table->increments('id');            
            $table->string('titulo');
            $table->string('mensaje');
            $table->string('rut_target');
            $table->foreign('rut_target')->references('rut')->on('users')->onDelete('cascade');
            $table->string('status');
            $table->dateTime('time')->default(DB::raw('CURRENT_TIMESTAMP'));            
        });

        Schema::create('notificacion_miembro_badge', function (Blueprint $table) {
            $table->unsignedInteger('notificacion_id');
            $table->foreign('notificacion_id')->references('id')->on('notificaciones')->onDelete('cascade');
            $table->string('monto');
            $table->string('rut_comercio');
            $table->foreign('rut_comercio')->references('rut')->on('comercios')->onDelete('cascade');                   
        });

        Schema::create('bloqueados', function (Blueprint $table) {
            $table->increments('id');
            $table->string('rut')->unique();
            $table->foreign('rut')->references('rut')->on('users')->onDelete('cascade');
            $table->string('blocked_by');
            $table->foreign('blocked_by')->references('rut')->on('users')->onDelete('cascade');
        });


        Schema::create('chats_deleted', function (Blueprint $table) {
            $table->increments('id');
            $table->string('rut');
            $table->foreign('rut')->references('rut')->on('users')->onDelete('cascade');
            $table->unsignedInteger('chat_id');     
            $table->foreign('chat_id')->references('id')->on('chat')->onDelete('cascade');        
        });

        Schema::create('messages_deleted', function (Blueprint $table) {
            $table->increments('id');     
            $table->string('rut');
            $table->foreign('rut')->references('rut')->on('users')->onDelete('cascade');
            $table->unsignedInteger('message_id');     
            $table->foreign('message_id')->references('id')->on('messages')->onDelete('cascade');
            $table->unsignedInteger('chat_id');     
            $table->foreign('chat_id')->references('id')->on('chat')->onDelete('cascade');              
        });

        DB::unprepared('
        CREATE TRIGGER crear_billetera_and_password_reset AFTER INSERT ON `users` FOR EACH ROW
            BEGIN
                INSERT INTO billetera (`rut`, `monto`,`pymcash`) 
                VALUES (NEW.rut, 0,0);
                INSERT INTO password_resets (`rut`, `email`) 
                VALUES (NEW.rut, NEW.email);
            END;
        ');

        DB::unprepared('
        CREATE TRIGGER actualizar_tiempo_chat AFTER INSERT ON `messages` FOR EACH ROW
            BEGIN
                UPDATE chat SET last_time = NEW.time WHERE id = NEW.chat_id;
            END;
        ');
        
        DB::unprepared('
        CREATE TRIGGER limpiar_mensajes_handle AFTER INSERT ON `messages_deleted` FOR EACH ROW
        BEGIN          

            IF NEW.message_id in (
            SELECT x.message_id 
            FROM messages_deleted x
            WHERE (NEW.message_id = x.message_id AND NEW.chat_id = x.chat_id AND NEW.rut <> x.rut)
            ) THEN
                DELETE FROM `messages` WHERE id = NEW.message_id;
            END IF;
        END;
        ');

        DB::unprepared('
        
        CREATE TRIGGER limpiar_chats_handle AFTER INSERT ON `chats_deleted` FOR EACH ROW
        BEGIN          

            IF NEW.chat_id in (
            SELECT x.chat_id 
            FROM chats_deleted x
            WHERE (NEW.chat_id = x.chat_id AND NEW.rut <> x.rut)
            ) THEN
                DELETE FROM `chat` WHERE id = NEW.chat_id;
            END IF;
        END;

        ');
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rol');
        Schema::dropIfExists('users');
        Schema::dropIfExists('comercios');
        Schema::dropIfExists('miembros');
        Schema::dropIfExists('billetera');
        Schema::dropIfExists('pymcash');
        Schema::dropIfExists('ventas');
        Schema::dropIfExists('movimientos_comercios');
        Schema::dropIfExists('movimientos_miembros');
        Schema::dropIfExists('transaccion');
        Schema::dropIfExists('chat');
        Schema::dropIfExists('messages');
        Schema::dropIfExists('notificaciones');
        Schema::dropIfExists('notificacion_miembro_badge');
        Schema::dropIfExists('bloqueados');
        Schema::dropIfExists('messages_deleted');
        Schema::dropIfExists('chats_deleted');
        DB::unprepared('DROP TRIGGER `crear_billetera_and_password_reset`');
        DB::unprepared('DROP TRIGGER `actualizar_tiempo_chat`');
        DB::unprepared('DROP TRIGGER `limpiar_mensajes_handle`');
        DB::unprepared('DROP TRIGGER `limpiar_chats_handle`');
        
    }
}
