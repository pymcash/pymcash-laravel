<?php

use Illuminate\Database\Seeder;
use App\User;
use Faker\Factory as Faker;
class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        


        DB::statement('DELETE FROM users WHERE id > 0');
        DB::statement('SET FOREIGN_KEY_CHECKS = 0;');
        User::truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS = 1;');
        User::create([
            'rut' => '2578695-9',
            'email' => 'microcybort@gmail.com',
            'password' => bcrypt('1'),
            'rol_id' => '2',
            'direccion' => 'Puerto La Cruz',
            'comuna' => 'example',
            'telefono' => '0424-00XXX',
            'latitud' => '10.431916',
            'longitud' => '-64.18331599999999',

        ])->setBilletera(1000)     
        ->Model_Create([
            'nombre' => 'Oscar',
            'apellido' => 'Farias',
        ]);

        User::create([
            'rut' => '2578695-8',
            'email' => 'microcybort2@gmail.com',
            'password' => bcrypt('1'),
            'rol_id' => '2',
            'direccion' => 'Puerto La Cruz',
            'comuna' => 'example',
            'telefono' => '0424-00XXX',
            'latitud' => '10.431916',
            'longitud' => '-64.18331599999999',

        ])->setBilletera(1000)     
        ->Model_Create([
            'nombre' => 'Jose',
            'apellido' => 'Perez',
        ]);


        User::create([
            'rut' => '26135619-8',
            'email' => 'johnsmith@gmail.com',
            'password' => bcrypt('1'),
            'rol_id' => '2',
            'direccion' => 'Av Salesianos 1400',
            'comuna' => 'San Miguel',
            'telefono' => '+56949975027',
            'latitud' => '-33.490485',
            'longitud' => '-70.657886',

        ])->setBilletera(1000)     
        ->Model_Create([
            'nombre' => 'John',
            'apellido' => 'Smith',
        ]);



        // COMERCIO
        User::create([
            'rut' => '12345678-9',
            'email' => 'example@gmail.com',
            'password' => bcrypt('1'),
            'rol_id' => '3',
            'direccion' => 'Puerto La Cruz',
            'comuna' => 'example',
            'telefono' => '0424-00XXX',
            'latitud' => '10.431916',
            'longitud' => '-64.18331599999999',
        ])->setBilletera(1000)     
        ->Model_Create([
            'nombre_fantasia' => 'DISTRIBUIDORA CA',
            'razon_social' => 'Distribucion de mercancia',
        ]);

       

        factory(App\User::class, 70)->create()->each(function($u) {
            $faker = Faker::create('es_PE');            
            $u->setBilletera(1500)
            ->Model_Create([
                'nombre_fantasia' => ucfirst($faker->company),
                'razon_social' => ucfirst($faker->bs),
            ]);
          });



    }
}
