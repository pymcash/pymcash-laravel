<?php

use Illuminate\Database\Seeder;
use App\Rol;
class RolSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0;');
        Rol::truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS = 1;');

        Rol::create(['name' => 'Administrador']);
        Rol::create(['name' => 'Miembro']);
        Rol::create(['name' => 'Comercio']);
        Rol::create(['name' => 'Desactivado']);
    }
}
