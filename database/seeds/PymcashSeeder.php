<?php

use Illuminate\Database\Seeder;
use App\Pymcash;
class PymcashSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0;');
        Pymcash::truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS = 1;');        
        Pymcash::create([
            'cant_moneda' => '1',
            'valor' => '50',
            'target_rol' => '2',
            'target' => '2000',
        ]);
        Pymcash::create([
            'cant_moneda' => '10',
            'valor' => '300',
            'target_rol' => '3',
            'target' => '0',
        ]);
    }
}
