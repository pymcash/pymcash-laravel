<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();
// VISTAS EXTERNAS DE APLICACION
Route::get('/','principal@index') ->name('index');
Route::get('/crearcuenta','principal@choose_create')->name('crear_cuenta'); 
Route::get('/crearmiembro','principal@crear_usuario')->name('crear_usuario'); 
Route::get('/crearcomercio','principal@crear_comercio')->name('crear_comercio'); 
Route::get('/iniciarsesion','principal@iniciar_sesion')->name('iniciar_sesion');
Route::get('/recuperarclave','principal@recuperar_password')->name('recuperar_password');

// VISTAS PROVICIONALES PANEL ADMINISTRADOR
Route::get('/adminpanel','admin@principaladmin')->name('principaladmin');
Route::get('/adminpanel/users','admin@adminusers')->name('adminusers');
Route::get('/adminpanel/users/edit','admin@adminusers_edit')->name('adminusers_edit');
Route::get('/adminpanel/commerces','admin@admincommerce')->name('admincommerce');
Route::get('/adminpanel/commerces/edit','admin@admincommerce_edit')->name('admincommerce_edit');

// VISTAS INTERNAS PARA MIEMBROS Y COMERCIOS

Route::middleware(['auth', 'role:Miembro,Comercio'])->group(function () {

    Route::get('/home','HomeController@mostrar_home')->name('home');
    Route::get('/billetera','HomeController@mostrar_billetera')->name('billetera');
    Route::get('/misdatos','HomeController@mostrar_datos')->name('mis_datos');
    Route::put('/misdatos','HomeController@editar_datos');
    Route::get('/localizar','HomeController@localizar')->name('localizar');
    Route::get('/mismovimientos','HomeController@mostrar_movimientos')->name('mis_movimientos');
    Route::get('/markers','HomeController@generar_markers')->name('markers');
    Route::get('/cambiarcontraseña','HomeController@mostrar_cambiar_contraseña')->name('cambiar_contraseña');
    Route::put('/cambiarcontraseña','HomeController@cambiar_contraseña');

    Route::get('/chat/{rut}','HomeController@mostrar_chat')->name('chat');
    Route::get('/chatlist','HomeController@chat_lista')->name('chat_lista');
    Route::get('/mensajeria','HomeController@mostrar_mensajeria')->name('mensajeria');

    Route::post('/insertar/{rut}','HomeController@insertar_mensaje')->name('insertar');
    Route::get('/notificaciones','HomeController@mostrar_notificaciones')->name('notificaciones');
    Route::get('/notificaciones/ver/{id}','UserController@notificacion_pymcash_compra_comercio')->name('ver_notificacion');
    Route::get('/notificaciones/elliminar/{id}','HomeController@eliminar_notificacion')->name('eliminar_notificacion');
    Route::get('/notificaciones/elliminartodo','HomeController@eliminar_notificaciones')->name('eliminar_notificaciones');
    Route::get('/sendmail','HomeController@send_mail');

    Route::get('/bloquear/{rut}','HomeController@bloquear')->name('bloquear');
    Route::get('/desbloquear/{rut}','HomeController@desbloquear')->name('desbloquear');
    Route::get('/mostrarbloqueados','HomeController@mostrar_bloqueados')->name('bloqueados');

    Route::get('/vaciarchat/{rut}','HomeController@vaciar_chat')->name('vaciar_chat');
    Route::get('/eliminarchat/{chatid}','HomeController@eliminar_chat')->name('eliminar_chat');


});

// VISTAS INTERNAS PARA MIEMBROS

Route::middleware(['auth', 'role:Miembro'])->group(function () {

    Route::get('/invitar','UserController@invitar_amigo')->name('invitar_amigo');
    
});    

// VISTAS INTERNAS PARA COMERCIOS
Route::middleware(['auth', 'role:Comercio'])->group(function () {

    Route::get('/vender','ComercioController@vender')->name('vender');
    Route::post('/vender','ComercioController@vender_action');
    Route::get('/comprar','ComercioController@comprar_pymcash')->name('comprar');    
    Route::get('/promocion','ComercioController@promocion')->name('promocion');
    Route::get('/promocion/mail','ComercioController@promocion_mail')->name('promocion_mail');
    Route::get('/promocion/chat','ComercioController@promocion_chat')->name('promocion_chat');
    Route::post('/promocion/chat/enviar','ComercioController@promocion_chat_enviar')->name('promocion_chat_enviar');

    Route::post('/comprar/vistaprevia','ComercioController@comprar_pymcash_previa')->name('previa');
    Route::get('/comprar/vistaprevia','ComercioController@comprar_pymcash');
    Route::get('/pagar','ComercioController@comprar_pymcash');


    Route::post('/pagar','TransbankController@transbank_preparar_compra_pymcash_pack')->name('pagar');

    //TRANSBANK
    Route::post('/procesando','TransbankController@transbank_procesar_compra')->name('procesando');
    Route::get('/procesando','HomeController@comprar_pymcash');
    
    Route::post('/finalizar','TransbankController@transbank_compra_finalizada')->name('compra_finalizada');
    Route::get('/finalizar','HomeController@comprar_pymcash');

    Route::get('/fallida','TransbankController@transbank_compra_fallida')->name('compra_fallida');



});    